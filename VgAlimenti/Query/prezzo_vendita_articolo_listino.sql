
CREATE TABLE [dbo].[prezzo_vendita_articolo_listino](
	[cod_mart] [varchar](15) NOT NULL,
	[data_reg] [smalldatetime] NOT NULL,
	[cod_vlis] [varchar](15) NULL,
	[promoNoPromo] [int] NULL,
	[prezzo_vendita] [float] NULL,
	[variazione_prezzo] [float] NULL
)

insert into prezzo_vendita_articolo_listino
SELECT cod_mart, t.d,
lisv.cod_vlis,
PromoNoPromo, 
lisv.prz_vend,
VariazionePrezzo
FROM vgalimenti.dbo.getTableDate('2016-01-01',GETDATE()) t 
cross apply (
	select lisv.cod_vlis,lisv.cod_mart,
	isNull(Promov.prz_vend, lisv.prz_vend) as prz_vend,
	case when PromoV.prz_vend is null then 0 else case when Promov.prz_vend > lisv.prz_vend then 0 else 1 end end as PromoNoPromo,
	case when PromoV.prz_vend is null then 0 else case when Promov.prz_vend > lisv.prz_vend then 0 else  case when lisv.prz_vend>0 
	then (lisv.prz_vend-Promov.prz_vend)/lisv.prz_vend else 0 end end end as VariazionePrezzo
	from  vgalimenti.dbo.getListinoVendita(t.d, null, null ) lisv 
			left outer join vgalimenti.dbo.getPromozioneVendita(t.d,t.d,null,null,null) promoV
			 ON lisv.cod_vlis = promov.cod_vlis and lisv.cod_mart = promoV.cod_mart
 )lisv
