with media_consegna as (

select  '10' as cod_mdep,codArticolo,codCliente,codPtoVend,
round(avg(convert (numeric(20,5),DATEDIFF(day,data_ord,dataDoc))) over (partition by codArticolo,codCliente,codPtoVend ),0) as lag_consegna_medio
from OLAPVenduto
where data_ord>='2017-01-01'
union 
select  '10' as cod_mdep,codArticolo,codCliente,codPtoVend,
round(avg(convert (numeric(20,5),DATEDIFF(day,data_ord,dataDoc))) over (partition by codArticolo,codCliente,codPtoVend ),0) as lag_consegna_medio
from OLAPVenduto_Salpar
where data_ord>='2017-01-01'
)
insert into consegne_medie_stimate
select distinct  cod_mdep,codArticolo,codCliente,codPtoVend,lag_consegna_medio
from media_consegna
order by  cod_mdep,codArticolo,codCliente,codPtoVend


|||||||||||||||||||||||||||||||||||||||||||||||||||NUOVA QUERY basata sulla moda||||||||||||||||||||||||||||||||
with media_consegna as (

select  '10' as cod_mdep,codArticolo,codCliente,codPtoVend,DATEDIFF(day,data_ord,dataDoc) as lag_consegna,
count(*) over (partition by codArticolo,codCliente,codPtoVend,DATEDIFF(day,data_ord,dataDoc) order by codArticolo,codCliente,codPtoVend ) as numero_eventi
from OLAPVenduto
where data_ord>='2017-01-01'
union 
select  '10' as cod_mdep,codArticolo,codCliente,codPtoVend,DATEDIFF(day,data_ord,dataDoc) as lag_consegna,
count(*) over (partition by codArticolo,codCliente,codPtoVend,DATEDIFF(day,data_ord,dataDoc) order by codArticolo,codCliente,codPtoVend ) as numero_eventi
from OLAPVenduto_Salpar
where data_ord>='2017-01-01'
)

,tmp as(
select cod_mdep,codArticolo,codCliente,codPtoVend,lag_consegna as lag_consegna_medio,numero_eventi,
ROW_NUMBER() over (partition by cod_mdep,codArticolo,codPtoVend order by cod_mdep,codArticolo,codPtoVend,numero_eventi,lag_consegna) as row_n
from media_consegna
)
,tmp1 as (
select cod_mdep,codArticolo,codCliente,codPtoVend,lag_consegna_medio,numero_eventi,row_n, max(row_n) over ( partition by cod_mdep,codArticolo,codCliente,codPtoVend) as max_row

from tmp 
)
insert into consegne_medie_stimate
select cod_mdep,codArticolo,codCliente,codPtoVend,lag_consegna_medio from tmp1 where row_n=max_row
order by codArticolo, codPtoVend



