CREATE TABLE prezzo_vendita_articolo(
	cod_mart varchar(15) not null,
	data_reg datetime not null,
	sum_promo int,
	avg_prezzo_vendita float,
	min_prezzo_vendita float,
	max_prezzo_vendita float,
	avg_variazione_prezzo float,
	min_variazione_prezzo float,
	max_variazione_prezzo float,
	stdev_variazione_prezzo float,

);

insert into prezzo_vendita_articolo

SELECT cod_mart, t.d, 
sum(PromoNoPromo) as sum_promo,
avg(lisv.prz_vend) as avg_prezzo_vendita,
min(lisv.prz_vend) as min_prezzo_vendita,
max(lisv.prz_vend) as max_prezzo_vendita,
avg(VariazionePrezzo) as avg_variazione_prezzo ,
min(VariazionePrezzo) as min_variazione_prezzo,
max(VariazionePrezzo) as max_variazione_prezzo,
stdev(VariazionePrezzo) as stdev_variazione_prezzo
FROM vgalimenti.dbo.getTableDate('2016-01-01',GETDATE()) t 
cross apply (
	select lisv.cod_mart,
	isNull(Promov.prz_vend, lisv.prz_vend) as prz_vend,
	case when PromoV.prz_vend is null then 0 else case when Promov.prz_vend > lisv.prz_vend then 0 else 1 end end as PromoNoPromo,
	case when PromoV.prz_vend is null then 0 else case when Promov.prz_vend > lisv.prz_vend then 0 else  case when lisv.prz_vend>0 
	then (lisv.prz_vend-Promov.prz_vend)/lisv.prz_vend else 0 end end end as VariazionePrezzo
	from  vgalimenti.dbo.getListinoVendita(t.d, null, null ) lisv 
			left outer join vgalimenti.dbo.getPromozioneVendita(t.d,t.d,null,null,null) promoV
			 ON lisv.cod_vlis = promov.cod_vlis and lisv.cod_mart = promoV.cod_mart
 )lisv

GROUP by t.d,cod_mart
--having min(lisv.prz_vend)>0
