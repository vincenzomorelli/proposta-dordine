
--query per la visualizzazione del grafico giallo ("rotture di stock da cambiare nome in inevaso")
select inevaso.cod_mart
     ,data_reg,sum(inevaso) as inevaso
     , sum(importo) as importo_inevaso
     , qta_esistente_cnf
     , case when qta_esistente_cnf <1 or sum(inevaso)/qta_cnf > qta_esistente_cnf then   'S' else 'N' end flag_inevaso
     , sum(inevaso)/qta_cnf as inevaso_cnf
from inevaso  join history_dashboard_data hdd
    on inevaso.cod_mart = hdd.cod_mart and inevaso.data_reg=dateadd(day,-1,hdd.data_predizione) and data_copertura=dateadd(day,2,data_predizione)
    join articoli_movimentati am on hdd.cod_mart = am.cod_mart
where inevaso.data_reg>'2017-2-01' and inevaso.cod_mart like '873201'
group by inevaso.cod_mart, data_reg, qta_esistente_cnf,data_copertura,data_predizione,qta_cnf

--valore di magazzino (qui devi calcolare il valore joinando la tabella che abbiamo creato insieme ieri e moltiplicando qta_esistente_cnf*qta_cnf*prezzo)
select storico_ordini.cod_mart,data_doc,qta_esistente_cnf,qta_cnf
from storico_ordini
    join articoli_movimentati on storico_ordini.cod_mart=articoli_movimentati.cod_mart
    join ( select distinct cod_mart,data_predizione,qta_esistente_cnf
        from history_dashboard_data) hdd on articoli_movimentati.cod_mart = hdd.cod_mart and storico_ordini.data_doc=hdd.data_predizione
order by cod_mart,data_doc
--affidabilità_fornitore /prodotto

--linea dell'affidabilita (quella verde) se riesci fai visualizzare anche il numero di ordini su cui è calcolata
select cod_mart, avg (case when qta_ord>qta_evasa then qta_evasa/qta_ord else 1 end)*100  as affidabilità, count(*) as numero_ordini
from storico_ordini
group by cod_mart


select cod_mart,datepart(weekday,data_ord) as giorno,count(datepart(weekday,data_ord)) as n_ordini
from storico_ordini
group by cod_mart,datepart(weekday,data_ord)


select cod_mart,datepart(weekday,data_doc) as giorno_scarichi,count(datepart(weekday,data_doc)) as n_scarichi
from storico_ordini
where data_doc is not null
group by cod_mart,datepart(weekday,data_doc)


select cod_mart,differenza from
(select  cod_mart,datediff(day,data_ord,data_doc) differenza, count(*) n_ordini, max(count(*)) over (partition by cod_mart) massimo_ordine
from storico_ordini
where  data_doc is not null
group by cod_mart,datediff(day,data_ord,data_doc)
    ) max_ord
where n_ordini=massimo_ordine
order by cod_mart,n_ordini desc
