set datefirst 1;
declare @date_input datetime;
declare @finestra int;
declare @cod_mart varchar(20);
set @date_input='2020-08-31';
set @finestra = 7;
set @cod_mart='138716';



with temp_giacenze_VG ([cod_mart],[cod_mdep],[data_reg],[qta_acq],[qta_venduta]) as (
	Select cod_mart,
	cod_mdep,
	'2016/01/01' as data_reg,
	qta_iniz+qta_car-qta_Scar as qta_acq,
	0.00 as qta_venduta

	from mtb_sart

	Where 
		anno = year('2015/01/01') and 
		qta_iniz+qta_car-qta_Scar > 0 and
		(qta_iniz+qta_car_costo) > 0 and cod_mart=@cod_mart  and cod_mdep in  ('10')
	
	UNION ALL

	Select cod_mart,cod_mdep,data_reg,qta_car,qta_scar
	from mtb_movi
	where data_reg>='2016-01-01' and cod_mart=@cod_mart  and cod_mdep in  ('10')
	
	)

,temp_giacenze_Salpar ([cod_mart],[cod_mdep],[data_reg],[qta_acq],[qta_venduta]) as 
(
	Select cod_mart,
	cod_mdep,
	'2016/01/01' as data_reg,
	qta_iniz+qta_car-qta_Scar as qta_acq,
	0.00 as qta_venduta

	from Vg_linked.Salpar.dbo.mtb_sart

	Where anno = year('2015/01/01') and 
			qta_iniz+qta_car-qta_Scar > 0 and
			(qta_iniz+qta_car_costo) > 0 and cod_mart=@cod_mart and cod_mdep in  ('10')
	
	UNION ALL

	Select cod_mart,cod_mdep,data_reg,qta_car,qta_scar
	from Vg_linked.Salpar.dbo.mtb_movi
	where data_reg>='2016-01-01' and cod_mart=@cod_mart and cod_mdep in  ('10')
	
	)

, tempgiacenze_VG as (
	select  isnull(cod_mart,@cod_mart) as cod_mart,
	'10' as cod_mdep,
	isnull(Dateadd(day,1,data_reg),Dateadd(day,1,v.d)) as data_reg,
	sum(sum(qta_acq-qta_venduta)) OVER (partition by  cod_mart order by  cod_mart, data_reg ) as giacenza_al_mattino
	
	From  vgalimenti.dbo.getTableDate('2016-01-01',GETDATE()) v left outer join  temp_giacenze_VG tg on d=data_reg

    and  data_reg<=@date_input

	group by  cod_mart,data_reg,d

)
,giacenze_VG as (
	SELECT
	  cod_mart, cod_mdep,data_reg, first_value(giacenza_al_mattino) over (partition by value_partition order by data_reg) as giacenza_al_mattino
	FROM (
	  SELECT
		 cod_mart, cod_mdep,data_reg,giacenza_al_mattino,
		sum(case when  giacenza_al_mattino is null then 0 else 1 end) over (order by data_reg) as value_partition

	  FROM tempgiacenze_VG
	) as q
)

, tempgiacenze_Salpar as (
	select  isnull(cod_mart,@cod_mart) as cod_mart,
	'10' as cod_mdep,
	isnull(Dateadd(day,1,data_reg),Dateadd(day,1,v.d)) as data_reg,
	sum(sum(qta_acq-qta_venduta)) OVER (partition by  cod_mart order by  cod_mart, data_reg ) as giacenza_al_mattino
	
	From  vgalimenti.dbo.getTableDate('2016-01-01',GETDATE()) v left outer join  temp_giacenze_Salpar tg on d=data_reg

    and  data_reg<=@date_input

	group by  cod_mart,data_reg,d

)
,giacenze_Salpar as (
	SELECT
	  cod_mart, cod_mdep,data_reg, first_value(giacenza_al_mattino) over (partition by value_partition order by data_reg) as giacenza_al_mattino
	FROM (
	  SELECT
		 cod_mart, cod_mdep,data_reg,giacenza_al_mattino,
		sum(case when  giacenza_al_mattino is null then 0 else 1 end) over (order by data_reg) as value_partition

	  FROM tempgiacenze_Salpar
	) as q
)
, in_consegna_VG as(	
	SELECT dtb_ordr.cod_mart,dtb_ordr.data_ord,dtb_ordr.data_cons,
		sum(dtb_ordr.qta_ord) as in_consegna

	FROM dtb_ordt,dtb_ordr 

	WHERE	( dtb_ordr.gestione = dtb_ordt.gestione ) and 
			( dtb_ordr.data_ord = dtb_ordt.data_ord ) and 
			( dtb_ordr.num_ord = dtb_ordt.num_ord ) and
            ( dtb_ordt.flag_annulla = 'N' ) and
            ( dtb_ordt.flag_sospeso = 'N' ) and
            ( dtb_ordr.flag_evaso <> 'A' ) and
            ( dtb_ordt.gestione = 'A' ) and
            dtb_ordt.data_ord <= Dateadd(day,-1,@date_input) 
			and  data_cons between @date_input 
			and dateadd(day, @finestra -1, @date_input) 		
			--and dtb_ordr.qta_ord - dtb_ordr.qta_evasa > 0 
			and dtb_ordr.cod_mart=@cod_mart

	GROUP BY dtb_ordr.cod_mart,data_cons,dtb_ordr.data_ord

	--having sum(dtb_ordr.qta_ord - dtb_ordr.qta_evasa) > 0
)
, in_consegna_Salpar as(	

	SELECT dtb_ordr.cod_mart,dtb_ordr.data_ord,dtb_ordr.data_cons,
		sum(dtb_ordr.qta_ord) as in_consegna

	FROM vg_linked.salpar.dbo.dtb_ordt,vg_linked.salpar.dbo.dtb_ordr 

	WHERE	( dtb_ordr.gestione = dtb_ordt.gestione ) and 
			( dtb_ordr.data_ord = dtb_ordt.data_ord ) and 
			( dtb_ordr.num_ord = dtb_ordt.num_ord ) and
            ( dtb_ordt.flag_annulla = 'N' ) and
            ( dtb_ordt.flag_sospeso = 'N' ) and
            ( dtb_ordr.flag_evaso <> 'A' ) and
            ( dtb_ordt.gestione = 'A' ) and
            dtb_ordt.data_ord <= Dateadd(day,-1,@date_input) 
			and  data_cons between @date_input and dateadd(day, @finestra -1, @date_input) 		
			--and dtb_ordr.qta_ord - dtb_ordr.qta_evasa > 0
			 and dtb_ordr.cod_mart=@cod_mart

	GROUP BY dtb_ordr.cod_mart,data_cons,dtb_ordr.data_ord

	--having sum(dtb_ordr.qta_ord - dtb_ordr.qta_evasa) > 0
)

,impegnato_VG as (	
	SELECT dtb_ordr.cod_mart,
			sum(dtb_ordr.qta_ord) as impegnato

	FROM	dtb_ordt,dtb_ordr 

	WHERE	( dtb_ordr.gestione = dtb_ordt.gestione ) and 
			( dtb_ordr.data_ord = dtb_ordt.data_ord ) and 
			( dtb_ordr.num_ord = dtb_ordt.num_ord ) and
            ( dtb_ordt.flag_annulla = 'N' ) and
            ( dtb_ordt.flag_sospeso = 'N' ) and
            ( dtb_ordr.flag_evaso <> 'A' ) and
            ( dtb_ordt.gestione = 'V' ) and
            dtb_ordt.data_ord <= Dateadd(day,-1,@date_input) and
            --dtb_ordr.qta_ord - dtb_ordr.qta_evasa > 0 and
            data_cons between @date_input and dateadd(day, @finestra -1, @date_input) and dtb_ordr.cod_mart=@cod_mart

	GROUP BY dtb_ordr.cod_mart

	--having sum(dtb_ordr.qta_ord - dtb_ordr.qta_evasa) > 0
	
)

,impegnato_Salpar as (	

	SELECT dtb_ordr.cod_mart,
			sum(dtb_ordr.qta_ord) as impegnato

	FROM  vg_linked.salpar.dbo.dtb_ordt, vg_linked.salpar.dbo.dtb_ordr 

	WHERE	( dtb_ordr.gestione = dtb_ordt.gestione ) and 
			( dtb_ordr.data_ord = dtb_ordt.data_ord ) and 
			( dtb_ordr.num_ord = dtb_ordt.num_ord ) and
            ( dtb_ordt.flag_annulla = 'N' ) and
            ( dtb_ordt.flag_sospeso = 'N' ) and
            ( dtb_ordr.flag_evaso <> 'A' ) and
            ( dtb_ordt.gestione = 'V' ) and
            dtb_ordt.data_ord <= Dateadd(day,-1,@date_input) and
            --dtb_ordr.qta_ord - dtb_ordr.qta_evasa > 0 and
            data_cons between @date_input and dateadd(day, @finestra -1, @date_input) and dtb_ordr.cod_mart=@cod_mart

	GROUP BY dtb_ordr.cod_mart

	--having sum(dtb_ordr.qta_ord - dtb_ordr.qta_evasa) > 0
	
)

select giacenze_VG.cod_mart,giacenze_VG.cod_mdep,
giacenze_VG.data_reg,isnull(giacenze_VG.giacenza_al_mattino,0) as giacenza_al_mattino_VG,
isnull(ic_VG.in_consegna,0) as in_consegna_VG,
isnull(im_VG.impegnato,0) as impegnato_VG,
isnull(giacenze_Salpar.giacenza_al_mattino,0) as giacenza_al_mattino_S,
isnull(ic_S.in_consegna,0) as in_consegna_S,
isnull(im_S.impegnato,0) as impegnato_S,
isnull(giacenze_VG.giacenza_al_mattino,0) - isnull(im_VG.impegnato,0) + isnull(ic_VG.in_consegna,0) +
isnull(giacenze_Salpar.giacenza_al_mattino,0) - isnull(im_S.impegnato,0) + isnull(ic_S.in_consegna,0) as disponibilitÓ

from giacenze_VG left join giacenze_Salpar on giacenze_VG.cod_mart = giacenze_Salpar.cod_mart and giacenze_VG.data_reg =giacenze_Salpar.data_reg
	left join  in_consegna_VG ic_VG on  ic_VG.cod_mart= giacenze_VG.cod_mart 
	left join  in_consegna_Salpar ic_S on  ic_S.cod_mart= giacenze_VG.cod_mart 
	left join impegnato_VG im_VG on im_VG.cod_mart= giacenze_VG.cod_mart
	left join impegnato_Salpar im_S on im_VG.cod_mart= giacenze_VG.cod_mart 
	
where giacenze_VG.data_reg=@date_input

