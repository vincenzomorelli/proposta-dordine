--create table giacenze_medie
with temp_giacenze_salpar ([cod_mart],[cod_mdep],[data_reg],[qta_acq],[qta_venduta]) as (
	Select cod_mart,
	cod_mdep,
	'2016/01/01' as data_reg,
	qta_iniz+qta_car-qta_Scar as qta_acq,
	0.00 as qta_venduta
	from vg_linked.salpar.dbo.mtb_sart
	Where
		anno = year('2015/01/01') and
		qta_iniz+qta_car-qta_Scar > 0 and
		(qta_iniz+qta_car_costo) > 0 --and cod_mart = @cod_mart
		and cod_mdep in  ('10')
	UNION ALL
	Select cod_mart,cod_mdep,data_reg,qta_car,qta_scar
	from vg_linked.salpar.dbo.mtb_movi
	where data_reg>='2016-01-01' --and cod_mart= @cod_mart
	and cod_mdep in  ('10')
	)
,temp_salpar_date as (
select distinct cod_mart ,v.d
from temp_giacenze_salpar cross apply ( select d from vgalimenti.dbo.getTableDate('2016-01-01',GETDATE())) v
)
, tempgiacenze_salpar as (
	select  tvg.cod_mart as cod_mart,
	'10' as cod_mdep,
	Dateadd(day,1,tvg.d) as data_reg,
	sum(sum(isnull(qta_acq,0)-isnull(qta_venduta,0))) OVER (partition by  tvg.cod_mart order by  tvg.cod_mart, tvg.d ) as giacenza_al_mattino
	--qta_acq,qta_venduta
	From  temp_salpar_date  tvg left outer join  temp_giacenze_salpar tg on d=data_reg and tvg.cod_mart=tg.cod_mart
    and  data_reg<='2021-01-08'
	group by  tvg.cod_mart,data_reg,d
	--order by tvg.cod_mart,tvg.d
)
,  temp_giacenze_VG ([cod_mart],[cod_mdep],[data_reg],[qta_acq],[qta_venduta]) as (
	Select cod_mart,
	cod_mdep,
	'2016/01/01' as data_reg,
	qta_iniz+qta_car-qta_Scar as qta_acq,
	0.00 as qta_venduta
	from mtb_sart
	Where
		anno = year('2015/01/01') and
		qta_iniz+qta_car-qta_Scar > 0 and
		(qta_iniz+qta_car_costo) > 0 --and cod_mart = @cod_mart
		and cod_mdep in  ('10')
	UNION ALL
	Select cod_mart,cod_mdep,data_reg,qta_car,qta_scar
	from mtb_movi
	where data_reg>='2016-01-01' --and cod_mart= @cod_mart
	and cod_mdep in  ('10')
	)

, temp_vg_date as (
select distinct cod_mart ,v.d
from temp_giacenze_VG cross apply ( select d from vgalimenti.dbo.getTableDate('2016-01-01',GETDATE())) v
)

, tempgiacenze_VG as (
	select  tvg.cod_mart as cod_mart,
	'10' as cod_mdep,
	Dateadd(day,1,tvg.d) as data_reg,
	sum(sum(isnull(qta_acq,0)-isnull(qta_venduta,0))) OVER (partition by  tvg.cod_mart order by  tvg.cod_mart, tvg.d ) as giacenza_al_mattino
	--qta_acq,qta_venduta
	From  temp_vg_date  tvg left outer join  temp_giacenze_VG tg on d=data_reg and tvg.cod_mart=tg.cod_mart
    and  data_reg<='2021-01-08'
	group by  tvg.cod_mart,data_reg,d
	--order by tvg.cod_mart,tvg.d
)

, giacenze_combinate as (
select 
		isnull(v.cod_mart, s.cod_mart) as cod_mart,
		isnull(v.data_reg, s.data_reg) as data_reg,
		isnull(s.giacenza_al_mattino,0)+isnull(v.giacenza_al_mattino,0) as giacenza_al_mattino
from tempgiacenze_VG v
full outer join tempgiacenze_salpar s on v.cod_mart = s.cod_mart and v.data_reg = s.data_reg
where (v.data_reg >= '2020-01-01' or s.data_reg >= '2020-01-01') 

)

insert into giacenza
select a.cod_mart,avg(giacenza_al_mattino) as giacenza_media
from giacenze_combinate a join(
select distinct cod_mart,data_ord,data_cons from vg_linked.salpar.dbo.dtb_ordr where gestione='A' and data_ord>'2020-01-01' and data_ord <='2020-09-30'
union
select distinct cod_mart,data_ord,data_cons from dtb_ordr where gestione='A'  and data_ord>'2020-01-01' and data_ord <='2020-09-30') b on a.cod_mart=b.cod_mart and a.data_reg=b.data_cons

where giacenza_al_mattino>0
group by a.cod_mart
order by cod_mart


