USE [vgalimenti_NN_project]
GO
/****** Object:  StoredProcedure [dbo].[update_prezzo_vendita_articolo_listino]    Script Date: 08/07/2020 13:29:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[update_prezzo_vendita_articolo_listino] (@periodo varchar(1), @newDepo varchar(1)) AS BEGIN
SET NOCOUNT ON;

DECLARE @dataIniz datetime, @codMdep varchar(5), @codVlis varchar(5), @dataFine datetime


SET @dataIniz = CAST(getDAte() AS date)

SELECT @dataIniz =  case @periodo 
					WHEN 'S' THEN DATEADD(WEEK, -2, @dataIniz)
					WHEN 'T' THEN'2016/01/01'
					ELSE @dataIniz END;

SELECT @dataFine =  DATEADD(MONTH, 1, Cast(getDAte() as date));


  declare @rc int; --record count
  set @rc = 1;
 
	while @rc > 0
	begin
			DELETE TOp (1000)
			FROM prezzo_vendita_articolo_listino
			WHERE data_reg >= @dataIniz


		set @rc = @@ROWCOUNT;
	end
	

;		while @dataIniz <= @dataFine
		begin

			
			insert into prezzo_vendita_articolo_listino
			SELECT cod_mart, t.d,
			lisv.cod_vlis,
			PromoNoPromo, 
			lisv.prz_vend,
			VariazionePrezzo
			FROM vgalimenti.dbo.getTableDate(@dataIniz,@dataIniz) t 
			cross apply (
				select lisv.cod_vlis,lisv.cod_mart,
				isNull(Promov.prz_vend, lisv.prz_vend) as prz_vend,
				case when PromoV.prz_vend is null then 0 else case when Promov.prz_vend > lisv.prz_vend then 0 else 1 end end as PromoNoPromo,
				case when PromoV.prz_vend is null then 0 else case when Promov.prz_vend > lisv.prz_vend then 0 else  case when lisv.prz_vend>0 
				then (lisv.prz_vend-Promov.prz_vend)/lisv.prz_vend else 0 end end end as VariazionePrezzo
				from  vgalimenti.dbo.getListinoVendita(t.d, null, null ) lisv 
						left outer join vgalimenti.dbo.getPromozioneVendita(t.d,t.d,null,null,null) promoV
						 ON lisv.cod_vlis = promov.cod_vlis and lisv.cod_mart = promoV.cod_mart
			 )lisv

			set @dataIniz = DATEADD(day, 1, @dataIniz )
		end



END
