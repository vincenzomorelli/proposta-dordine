CREATE TABLE ricorrenze (
   data_ricorrenza datetime not null primary key
);
--popola ricorrenze nel periodo considerato
insert into ricorrenze
select distinct d as data_ricorrenza
from vgalimenti.dbo.getTableDate('2016/01/01','2020/12/31') t
inner join vgalimenti.dbo.jtb_ricorrenze r on t.d between DATEADD(dd, -2, r.data) and r.data