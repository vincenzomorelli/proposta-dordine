USE [vgalimenti_NN_project]
GO
/****** Object:  StoredProcedure [dbo].[get_predizioni]    Script Date: 24/06/2021 18:55:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[get_predizioni] @finestra int
AS

declare @date_input datetime;
set @date_input = convert(date, getdate());

with pred_14 as (
select 
		cod_mart, 
		data_predizione, 
		sum(predizione) as predizione_14
from Predizione_14 
where data_predizione=@date_input 
	and data_reg >= Dateadd(day,8,data_predizione) and data_reg <=  Dateadd(day,@finestra,data_predizione)
	and @finestra > 7

group by cod_mart, data_predizione
)
,temp_predizioni as (
select distinct cod_mart ,v.d data_reg
from Predizione cross apply ( select d from vgalimenti.dbo.getTableDate(dateadd(day,1,@date_input),Dateadd(day,7,@date_input))) v
where data_predizione=@date_input
)
, pred_giornaliera as (
select 
		a.cod_mart, 
		isnull(data_predizione,@date_input) data_predizione,
		a.data_reg,
		sum(predizione) over (partition by a.cod_mart order by a.data_reg rows unbounded preceding)/(sum(predizione) over (partition by a.cod_mart)+1) as coeff

from temp_predizioni a  left  join Predizione b on a.cod_mart=b.cod_mart and a.data_reg=b.data_reg and data_predizione=@date_input and b.data_reg <= Dateadd(day,7,data_predizione) 
where 
	   a.data_reg>=@date_input
	
)
insert into vgalimenti_NN_project.dbo.temp_final_table
select
		ps.cod_mart, 
		ps.data_predizione,
		@finestra as finestra,	
		--ps.predizione as predizione_settimana,
		--predizione_giorno,
		--predizione_14,
		case when @finestra = 7 then ps.predizione
			 when @finestra < 7 then ps.predizione * coeff
			 when @finestra > 7 then ps.predizione + isnull(predizione_14,0) end as predizione  
from Predizione_settimanale ps 
left join pred_giornaliera p on p.cod_mart = ps.cod_mart and p.data_predizione = ps.data_predizione and p.data_reg=Dateadd(day,@finestra,@date_input)
left join pred_14 pp on pp.cod_mart = ps.cod_mart and pp.data_predizione = ps.data_predizione
where ps.data_predizione=@date_input 

