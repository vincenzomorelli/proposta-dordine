
CREATE PROCEDURE get_impegnato_consegna @finestra int
AS

declare @date_input datetime;
set @date_input = convert(date, getdate());

with in_consegna_VG as(	
	SELECT dtb_ordr.cod_mart,--dtb_ordr.data_ord,dtb_ordr.data_cons,
		sum(dtb_ordr.qta_ord) as in_consegna

	FROM dtb_ordt,dtb_ordr 

	WHERE	( dtb_ordr.gestione = dtb_ordt.gestione ) and 
			( dtb_ordr.data_ord = dtb_ordt.data_ord ) and 
			( dtb_ordr.num_ord = dtb_ordt.num_ord ) and
            ( dtb_ordt.flag_annulla = 'N' ) and
            ( dtb_ordt.flag_sospeso = 'N' ) and
            ( dtb_ordr.flag_evaso <> 'A' ) and
            ( dtb_ordt.gestione = 'A' ) and
            dtb_ordt.data_ord <= Dateadd(day,-1,@date_input) 
			and  data_cons between @date_input
			and dateadd(day, @finestra -1, @date_input) 		
			--and dtb_ordr.cod_mart in (select distinct cod_mart from predizioni_2020_fullWeek)
			--and dtb_ordr.cod_mart='165507'

	GROUP BY dtb_ordr.cod_mart--,data_cons,dtb_ordr.data_ord
)

, in_consegna_salpar as (
SELECT	a.cod_mart,
			--a.data_ord  ,
			--a.data_cons ,
			sum(a.qta_ord) as in_consegna

	FROM vg_linked.salpar.dbo.dtb_ordt,vg_linked.salpar.dbo.dtb_ordr a

	WHERE	( a.gestione = dtb_ordt.gestione ) and 
			( a.data_ord = dtb_ordt.data_ord ) and 
			( a.num_ord = dtb_ordt.num_ord ) and
            ( dtb_ordt.flag_annulla = 'N' ) and
            ( dtb_ordt.flag_sospeso = 'N' ) and
            ( a.flag_evaso <> 'A' ) and
            ( dtb_ordt.gestione = 'A' ) and
            dtb_ordt.data_ord <= Dateadd(day,-1,@date_input) 
			and  data_cons between @date_input
			and dateadd(day, 7-1, @date_input) 		
			     --and dtb_ordr.qta_ord - dtb_ordr.qta_evasa > 0 
			--and a.cod_mart in (select distinct cod_mart from predizioni_2020_fullWeek)
			
	GROUP BY a.cod_mart,a.data_cons,a.data_ord
)

,impegnato_VG as (	
	SELECT dtb_ordr.cod_mart,
			--dtb_ordr.data_ord ,
			sum(dtb_ordr.qta_ord) as impegnato

	FROM	dtb_ordt,dtb_ordr 

	WHERE	( dtb_ordr.gestione = dtb_ordt.gestione ) and 
			( dtb_ordr.data_ord = dtb_ordt.data_ord ) and 
			( dtb_ordr.num_ord = dtb_ordt.num_ord ) and
            ( dtb_ordt.flag_annulla = 'N' ) and
            ( dtb_ordt.flag_sospeso = 'N' ) and
            ( dtb_ordr.flag_evaso <> 'A' ) and
            ( dtb_ordt.gestione = 'V' ) and
            dtb_ordt.data_ord <= Dateadd(day,-1,@date_input) and
            --dtb_ordr.qta_ord - dtb_ordr.qta_evasa > 0 and
            data_cons between @date_input and dateadd(day, @finestra -1, @date_input) --and dtb_ordr.cod_mart=@cod_mart
			--and cod_mart in (select distinct cod_mart from predizioni_2020_fullWeek)
	GROUP BY dtb_ordr.cod_mart
	
)
, impegnato_salpar as (

	SELECT c.cod_mart,
			--c.data_ord,
			sum(c.qta_ord) as impegnato

	FROM	vg_linked.salpar.dbo.dtb_ordt,vg_linked.salpar.dbo.dtb_ordr c

	WHERE	( c.gestione = dtb_ordt.gestione ) and 
			( c.data_ord = dtb_ordt.data_ord ) and 
			( c.num_ord = dtb_ordt.num_ord ) and
            ( dtb_ordt.flag_annulla = 'N' ) and
            ( dtb_ordt.flag_sospeso = 'N' ) and
            ( c.flag_evaso <> 'A' ) and
            ( dtb_ordt.gestione = 'V' ) and
            dtb_ordt.data_ord <= Dateadd(day,-1,@date_input) and
                 --dtb_ordr.qta_ord - dtb_ordr.qta_evasa > 0 and
            data_cons between @date_input and dateadd(day, 7 -1, @date_input) --and dtb_ordr.cod_mart=@cod_mart
			--and cod_mart in (select distinct cod_mart from predizioni_2020_fullWeek)
	GROUP BY c.cod_mart 
)

, impegnato_combinato as (
select 
		isnull(v.cod_mart, s.cod_mart) as cod_mart,
		isnull(s.impegnato,0)+isnull(v.impegnato,0) as impegnato
from impegnato_VG v 
full outer join impegnato_salpar s on s.cod_mart = v.cod_mart
)

, in_consegna_combinato as (
select 
		isnull(v.cod_mart, s.cod_mart) as cod_mart,
		isnull(s.in_consegna,0)+isnull(v.in_consegna,0) as in_consegna
from in_consegna_VG v 
full outer join in_consegna_salpar s on s.cod_mart = v.cod_mart
)

, giacenza_mattino as (
select 
		isnull(v.cod_mart, s.cod_mart) as cod_mart,
		isnull(v.qta_esistente, 0) + isnull(s.qta_esistente, 0) as qta_esistente
from vg_linked.vgalimenti.dbo.mtb_part v
full outer join vg_linked.salpar.dbo.mtb_part s on s.cod_mart = v.cod_mart
where (v.cod_mdep = '10' and s.cod_mdep = '10') or (s.cod_mdep = '10' and v.cod_mdep is null) or  (v.cod_mdep = '10' and s.cod_mdep is null)
--order by cod_mart
)

insert into impegnato_consegna_provvisorio
SELECT 
	distinct ps.cod_mart, 
	ps.data_predizione,
	@finestra as finestra,
	isnull(i.impegnato,0) as impegnato, 
	isnull(c.in_consegna,0) as in_consegna,
	ma.qta_esistente, 
	ma.qta_esistente-isnull(i.impegnato,0)+isnull(c.in_consegna,0) as disponibilita 
from Predizione_settimanale ps 
left join giacenza_mattino ma on ma.cod_mart = ps.cod_mart
left join impegnato_combinato i on i.cod_mart = ps.cod_mart 
left join in_consegna_combinato c on ps.cod_mart = c.cod_mart 
where data_predizione = @date_input
