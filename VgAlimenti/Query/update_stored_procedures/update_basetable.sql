use vgalimenti_NN_project

delete from mtb_aart
insert into mtb_aart 
select *  
from vg_linked.vgalimenti.dbo.mtb_aart a 


insert into salpar.dbo.mtb_aart 
select *  
from vg_linked.salpar.dbo.mtb_aart a 
where cod_mart not in (select cod_mart from salpar.dbo.mtb_aart)

delete from vtb_clie
insert into vtb_clie
select *  
from vg_linked.vgalimenti.dbo.vtb_clie a 
--where a.cod_anag not in  ( select cod_anag  from vtb_clie)


delete from vtb_dest
insert into vtb_dest 
select *  
from vg_linked.vgalimenti.dbo.vtb_dest a 
--where not exists (select 1 from vtb_dest b where a.cod_anag = b.cod_anag and a.cod_vdes = b.cod_vdes)



delete from vtb_ord_giror
insert into vtb_ord_giror 
select * 
from vg_linked.vgalimenti.dbo.vtb_ord_giror


delete from mtb_comp
insert into mtb_comp 
select * 
from vg_linked.vgalimenti.dbo.mtb_comp


delete from vtb_list
insert into vtb_list 
select * 
from vg_linked.vgalimenti.dbo.vtb_list a 


delete from mtb_lisv_data
insert into mtb_lisv_data
select * 
from vg_linked.vgalimenti.dbo.mtb_lisv_data a
--where not exists (select 1 from mtb_lisv_data b where a.cod_mart = b.cod_mart and a.cod_vlis = b.cod_vlis and a.versione=b.versione)

delete from vtb_promo
insert into vtb_promo
select * 
from vg_linked.vgalimenti.dbo.vtb_promo


delete from mtb_movi 
where data_reg>= Dateadd( day ,-7,GETDATE ( ))

insert into mtb_movi 
select *
from vg_linked.vgalimenti.dbo.mtb_movi
where data_reg>= Dateadd( day ,-7,GETDATE ( ))



delete from vtb_list_data 

insert into vtb_list_data
select * 
from vg_linked.vgalimenti.dbo.vtb_list_data


delete from mtb_grup
insert into mtb_grup
select *
from vg_linked.vgalimenti.dbo.mtb_grup



delete from mtb_sgrp
insert into mtb_sgrp
select *
from vg_linked.vgalimenti.dbo.mtb_sgrp


delete from mtb_sfam
insert into mtb_sfam
select *
from vg_linked.vgalimenti.dbo.mtb_sfam



delete from dtb_ordr 
where data_ord>= Dateadd( day ,-15,GETDATE ( ))

insert into dtb_ordr
select * 
from vg_linked.vgalimenti.dbo.dtb_ordr
where data_ord>= Dateadd( day ,-15,GETDATE ( ))



delete from dtb_ordt
where data_ord>= Dateadd( day ,-15,GETDATE ( ))

insert into dtb_ordt
select * 
from vg_linked.vgalimenti.dbo.dtb_ordt
where data_ord>= Dateadd( day ,-15,GETDATE ( ))


delete from salpar.dbo.dtb_ordr 
where data_ord>= Dateadd( day ,-15,GETDATE ( ))

insert into salpar.dbo.dtb_ordr
select * 
from vg_linked.salpar.dbo.dtb_ordr
where data_ord>= Dateadd( day ,-15,GETDATE ( ))



delete from salpar.dbo.dtb_ordt
where data_ord>= Dateadd( day ,-15,GETDATE ( ))

insert into  salpar.dbo.dtb_ordt
select * 
from vg_linked.salpar.dbo.dtb_ordt
where data_ord>= Dateadd( day ,-15,GETDATE ( ))

/*delete from vgalimenti.dbo.mtb_part
insert into vgalimenti.dbo.mtb_part
select * 
from vg_linked.vgalimenti.dbo.mtb_part

delete from salpar.dbo.mtb_part
insert into salpar.dbo.mtb_part
select * 
from vg_linked.salpar.dbo.mtb_part*/

delete from OLAPVenduto
where dataDoc>=Dateadd( day ,-15,GETDATE ( ))

insert into OlapVenduto
select 	dtb_doct.cod_dtip,
	dtb_doct.data_doc as dataDoc,
	isNull(isNull(dtb_docr.data_ord, dtb_doct.data_ord), dateadd(day,-1,dtb_doct.data_doc)) as data_ord,
	dtb_doct.cod_mdep AS codDeposito,
	dtb_doct.cod_dtip as codTipoDoc,
	dtb_doct.cod_anag as codCliente,
	isnull(dtb_doct.cod_vdes,right(dtb_doct.cod_anag,4)) as codPtoVend,
	isNull(dtb_doct.cod_vzon, 'NoDef') as codAreaCom,
	isNull(dtb_doct.cod_vage, 'NoDef') as CodAgente,
	isNull(dtb_doct.cod_vvet, 'NoDef') as CodConsegnatario,
	isNull(dtb_doct.cod_auto,'NoDef') as CodAutomezzo,
	CASE WHEN dtb_docr.sconto8 = 100 THEN 'Sost' ELSE isNull(dtb_docr.cod_promo, 'NoDef')END as IDPromo,
	dtb_docr.cod_mart as codArticolo,
	CASE WHEN dtb_tipi.segno_qta_scar = 1 AND dtb_docr.importo_riga <> 0 THEN dtb_docr.qta_doc ELSE 0.0 END AS QuantitaVenduta,
	CASE WHEN dtb_tipi.segno_qta_scar = 1 AND dtb_docr.importo_riga <> 0  THEN dtb_docr.qta_doc/dtb_docr.qta_cnf ELSE 0.0 END AS ConfVendute,
	CASE WHEN dtb_tipi.segno_qta_scar = 1 AND dtb_docr.importo_riga <> 0 THEN dtb_docr.qta_doc/mtb_aart.rap_conv2 ELSE 0.0 END AS QtaStatVenduta,
	CASE WHEN dtb_tipi.segno_qta_scar = 1 AND dtb_docr.sconto8 = 100 THEN dtb_docr.qta_doc ELSE 0.0 END AS QuantitaSostituita,
	CASE WHEN dtb_tipi.segno_qta_scar = 1 AND dtb_docr.sconto8 = 100 THEN dtb_docr.qta_doc/mtb_aart.rap_conv2 ELSE 0.0 END AS QuantitaStatSost,
	CASE WHEN dtb_tipi.segno_qta_scar = -1 THEN dtb_docr.qta_doc ELSE 0.0 END AS QuantitaResa,
	CASE WHEN dtb_tipi.segno_qta_scar = -1 THEN dtb_docr.qta_doc/dtb_docr.qta_cnf ELSE 0.0 END AS ConfRese,
	CASE WHEN dtb_tipi.segno_qta_scar = -1  THEN dtb_docr.qta_doc/mtb_aart.rap_conv2 ELSE 0.0 END AS QtaStatResa,
	CASE WHEN dtb_tipi.segno_qta_scar = 1 AND dtb_docr.importo_riga = 0 THEN dtb_docr.qta_doc ELSE 0.0 END AS QuantitaOmaggi,
	CASE WHEN dtb_tipi.segno_qta_scar = 1 AND dtb_docr.importo_riga = 0 THEN dtb_docr.qta_doc/dtb_docr.qta_cnf ELSE 0.0 END AS ConfOmaggio,
	CASE WHEN dtb_tipi.segno_qta_scar = 1 AND dtb_docr.importo_riga = 0 THEN dtb_docr.qta_doc/mtb_aart.rap_conv2 ELSE 0.0 END AS QtaStatOmaggi,
	CASE WHEN dtb_tipi.segno_val_scar = 1  AND dtb_docr.sconto5 <> 100
                 THEN dtb_docr.importo_riga+
                       ABS((dtb_docr.qta_doc*dtb_docr.val_unt*dtb_docr.perc_gest/100)+(dtb_docr.qta_doc*dtb_docr.val_gest)) +
                       ABS((dtb_docr.qta_doc*dtb_docr.val_unt*dtb_docr.perc_promo/100)+(dtb_docr.qta_doc*dtb_docr.val_promo)) 
		 ELSE 0.0 END AS ValoreVenduto,
	CASE WHEN dtb_tipi.segno_val_scar = -1  AND dtb_docr.sconto5 <> 100
		THEN dtb_docr.importo_riga + 
			ABS((dtb_docr.qta_doc*dtb_docr.val_unt*dtb_docr.perc_gest/100)+(dtb_docr.qta_doc*dtb_docr.val_gest)) +
			ABS((dtb_docr.qta_doc*dtb_docr.val_unt*dtb_docr.perc_promo/100)+(dtb_docr.qta_doc*dtb_docr.val_promo)) 
		ELSE 0.0 END AS ValoreReso,
	CASE WHEN dtb_tipi.segno_val_scar = 1  and dtb_docr.sconto5 <> 100
                 THEN ABS((dtb_docr.qta_doc*dtb_docr.val_unt*dtb_docr.perc_gest/100)+(dtb_docr.qta_doc*dtb_docr.val_gest)) +
                            ABS((dtb_docr.qta_doc*dtb_docr.val_unt*dtb_docr.perc_promo/100)+(dtb_docr.qta_doc*dtb_docr.val_promo)) 
		 ELSE 0.0 END AS CompensiSuVenduto,
	CASE WHEN dtb_tipi.segno_val_scar = -1  and dtb_docr.sconto5 <> 100 
		THEN   ABS((dtb_docr.qta_doc*dtb_docr.val_unt*dtb_docr.perc_gest/100)+(dtb_docr.qta_doc*dtb_docr.val_gest)) +
			ABS((dtb_docr.qta_doc*dtb_docr.val_unt*dtb_docr.perc_promo/100)+(dtb_docr.qta_doc*dtb_docr.val_promo)) 
		ELSE 0.0 END AS CompensiSuReso,
	CASE WHEN dtb_tipi.segno_qta_scar = 1 and dtb_docr.importo_riga <> 0  THEN dtb_docr.qta_doc * dtb_docr.costo_unt ELSE 0.0 END AS CostoVenduto,
	CASE WHEN dtb_tipi.segno_qta_scar = -1 and dtb_docr.importo_riga <> 0 THEN dtb_docr.qta_doc * dtb_docr.costo_unt ELSE 0.0 END AS CostoResi,
	CASE WHEN dtb_tipi.segno_qta_scar = 1 AND dtb_docr.importo_riga = 0 AND dtb_docr.sconto8 <> 100 THEN dtb_docr.qta_doc * dtb_docr.costo_unt ELSE 0.0 END AS CostoOmaggi,
	/*N.B.  and mtb_aart.cod_mgrp <> '2' il gruppo merceologico 2 corrisponde alla divisione PARMLAT per la quale gli omaggi vengono rimborasati*/
	CASE WHEN dtb_tipi.segno_qta_scar = 1 AND dtb_docr.sconto8 = 100  THEN dtb_docr.qta_doc * dtb_docr.costo_unt ELSE 0.0 END AS CostoSost,
	dtb_tipi.segno_val_scar * (dtb_Docr.importo_riga * dtb_docr.perc_prov/100 + dtb_docr.qta_doc * dtb_docr.val_prov) as Provvigioni,
	dtb_tipi.segno_val_scar *   (dtb_docr.importo_riga +
                       ABS((dtb_docr.qta_doc*dtb_docr.val_unt*dtb_docr.perc_gest/100)+(dtb_docr.qta_doc*dtb_docr.val_gest)) +
                       ABS((dtb_docr.importo_riga*dtb_docr.perc_promo/100)+(dtb_docr.qta_doc*dtb_docr.val_promo)) ) as FatturatoNetto,
             dtb_tipi.segno_qta_scar * dtb_docr.qta_doc * dtb_docr.costo_unt as CostoVendutoNetto,
	dtb_tipi.segno_val_scar *   (dtb_docr.importo_riga +
                       ABS((dtb_docr.qta_doc*dtb_docr.val_unt*dtb_docr.perc_gest/100)+(dtb_docr.qta_doc*dtb_docr.val_gest)) +
                       ABS((dtb_docr.importo_riga*dtb_docr.perc_promo/100)+(dtb_docr.qta_doc*dtb_docr.val_promo)) ) -
             (dtb_tipi.segno_qta_scar * dtb_docr.qta_doc * dtb_docr.costo_unt) as MargineLordo



from [VG_LINKED].[vgalimenti].[dbo].dtb_doct, [VG_LINKED].[vgalimenti].[dbo].dtb_tipi, [VG_LINKED].[vgalimenti].[dbo].dtb_docr, [VG_LINKED].[vgalimenti].[dbo].mtb_aart
where 
dtb_doct.cod_anag = dtb_docr.cod_anag and
dtb_doct.cod_dtip = dtb_docr.cod_dtip and
dtb_doct.data_doc = dtb_docr.data_doc and
dtb_doct.ser_doc = dtb_docr.ser_doc and
dtb_doct.num_doc = dtb_docr.num_doc and
dtb_docr.cod_mart = mtb_aart.cod_mart and
dtb_doct.cod_dtip = dtb_tipi.cod_dtip and dtb_doct.gestione = 'V' and
dtb_tipi.tipo_emissione = 'DIRETTA' and
dtb_doct.data_doc >= Dateadd( day ,-15,GETDATE ( )) and
dtb_tipi.flag_incl_stat = 'S'




delete from OLAPVenduto_Salpar
where dataDoc>=Dateadd( day ,-15,GETDATE ( ))

insert into OLAPVenduto_Salpar
select 	dtb_doct.cod_dtip,
	dtb_doct.data_doc as dataDoc,
	isNull(isNull(dtb_docr.data_ord, dtb_doct.data_ord), dateadd(day,-1,dtb_doct.data_doc)) as data_ord,
	dtb_doct.cod_mdep AS codDeposito,
	dtb_doct.cod_dtip as codTipoDoc,
	dtb_doct.cod_anag as codCliente,
	isnull(dtb_doct.cod_vdes,right(dtb_doct.cod_anag,4)) as codPtoVend,
	isNull(dtb_doct.cod_vzon, 'NoDef') as codAreaCom,
	isNull(dtb_doct.cod_vage, 'NoDef') as CodAgente,
	isNull(dtb_doct.cod_vvet, 'NoDef') as CodConsegnatario,
	isNull(dtb_doct.cod_auto,'NoDef') as CodAutomezzo,
	CASE WHEN dtb_docr.sconto8 = 100 THEN 'Sost' ELSE isNull(dtb_docr.cod_promo, 'NoDef')END as IDPromo,
	dtb_docr.cod_mart as codArticolo,
	CASE WHEN dtb_tipi.segno_qta_scar = 1 AND dtb_docr.importo_riga <> 0 THEN dtb_docr.qta_doc ELSE 0.0 END AS QuantitaVenduta,
	CASE WHEN dtb_tipi.segno_qta_scar = 1 AND dtb_docr.importo_riga <> 0  THEN dtb_docr.qta_doc/dtb_docr.qta_cnf ELSE 0.0 END AS ConfVendute,
	CASE WHEN dtb_tipi.segno_qta_scar = 1 AND dtb_docr.importo_riga <> 0 THEN dtb_docr.qta_doc/mtb_aart.rap_conv2 ELSE 0.0 END AS QtaStatVenduta,
	CASE WHEN dtb_tipi.segno_qta_scar = 1 AND dtb_docr.sconto8 = 100 THEN dtb_docr.qta_doc ELSE 0.0 END AS QuantitaSostituita,
	CASE WHEN dtb_tipi.segno_qta_scar = -1 THEN dtb_docr.qta_doc ELSE 0.0 END AS QuantitaResa,
	CASE WHEN dtb_tipi.segno_qta_scar = -1 THEN dtb_docr.qta_doc/dtb_docr.qta_cnf ELSE 0.0 END AS ConfRese,
	CASE WHEN dtb_tipi.segno_qta_scar = -1  THEN dtb_docr.qta_doc/mtb_aart.rap_conv2 ELSE 0.0 END AS QtaStatResa,
	CASE WHEN dtb_tipi.segno_qta_scar = 1 AND dtb_docr.importo_riga = 0 THEN dtb_docr.qta_doc ELSE 0.0 END AS QuantitaOmaggi,
	CASE WHEN dtb_tipi.segno_qta_scar = 1 AND dtb_docr.importo_riga = 0 THEN dtb_docr.qta_doc/dtb_docr.qta_cnf ELSE 0.0 END AS ConfOmaggio,
	CASE WHEN dtb_tipi.segno_qta_scar = 1 AND dtb_docr.importo_riga = 0 THEN dtb_docr.qta_doc/mtb_aart.rap_conv2 ELSE 0.0 END AS QtaStatOmaggi,
	CASE WHEN dtb_tipi.segno_val_scar = 1  AND dtb_docr.sconto5 <> 100
                 THEN dtb_docr.importo_riga+
                       ABS((dtb_docr.qta_doc*dtb_docr.val_unt*dtb_docr.perc_gest/100)+(dtb_docr.qta_doc*dtb_docr.val_gest)) +
                       ABS((dtb_docr.qta_doc*dtb_docr.val_unt*dtb_docr.perc_promo/100)+(dtb_docr.qta_doc*dtb_docr.val_promo)) 
		 ELSE 0.0 END AS ValoreVenduto,
	CASE WHEN dtb_tipi.segno_val_scar = -1  AND dtb_docr.sconto5 <> 100
		THEN dtb_docr.importo_riga + 
			ABS((dtb_docr.qta_doc*dtb_docr.val_unt*dtb_docr.perc_gest/100)+(dtb_docr.qta_doc*dtb_docr.val_gest)) +
			ABS((dtb_docr.qta_doc*dtb_docr.val_unt*dtb_docr.perc_promo/100)+(dtb_docr.qta_doc*dtb_docr.val_promo)) 
		ELSE 0.0 END AS ValoreReso,
	CASE WHEN dtb_tipi.segno_val_scar = 1  and dtb_docr.sconto5 <> 100
                 THEN ABS((dtb_docr.qta_doc*dtb_docr.val_unt*dtb_docr.perc_gest/100)+(dtb_docr.qta_doc*dtb_docr.val_gest)) +
                            ABS((dtb_docr.qta_doc*dtb_docr.val_unt*dtb_docr.perc_promo/100)+(dtb_docr.qta_doc*dtb_docr.val_promo)) 
		 ELSE 0.0 END AS CompensiSuVenduto,
	CASE WHEN dtb_tipi.segno_val_scar = -1  and dtb_docr.sconto5 <> 100 
		THEN   ABS((dtb_docr.qta_doc*dtb_docr.val_unt*dtb_docr.perc_gest/100)+(dtb_docr.qta_doc*dtb_docr.val_gest)) +
			ABS((dtb_docr.qta_doc*dtb_docr.val_unt*dtb_docr.perc_promo/100)+(dtb_docr.qta_doc*dtb_docr.val_promo)) 
		ELSE 0.0 END AS CompensiSuReso,
	CASE WHEN dtb_tipi.segno_qta_scar = 1 and dtb_docr.importo_riga <> 0  THEN dtb_docr.qta_doc * dtb_docr.costo_unt ELSE 0.0 END AS CostoVenduto,
	CASE WHEN dtb_tipi.segno_qta_scar = -1 and dtb_docr.importo_riga <> 0 THEN dtb_docr.qta_doc * dtb_docr.costo_unt ELSE 0.0 END AS CostoResi,
	CASE WHEN dtb_tipi.segno_qta_scar = 1 AND dtb_docr.importo_riga = 0 AND dtb_docr.sconto8 <> 100 THEN dtb_docr.qta_doc * dtb_docr.costo_unt ELSE 0.0 END AS CostoOmaggi,
	/*N.B.  and mtb_aart.cod_mgrp <> '2' il gruppo merceologico 2 corrisponde alla divisione PARMLAT per la quale gli omaggi vengono rimborasati*/
	dtb_tipi.segno_val_scar * (dtb_Docr.importo_riga * dtb_docr.perc_prov/100 + dtb_docr.qta_doc * dtb_docr.val_prov) as Provvigioni,
	dtb_tipi.segno_val_scar *   (dtb_docr.importo_riga +
                       ABS((dtb_docr.qta_doc*dtb_docr.val_unt*dtb_docr.perc_gest/100)+(dtb_docr.qta_doc*dtb_docr.val_gest)) +
                       ABS((dtb_docr.importo_riga*dtb_docr.perc_promo/100)+(dtb_docr.qta_doc*dtb_docr.val_promo)) ) as FatturatoNetto,
             dtb_tipi.segno_qta_scar * dtb_docr.qta_doc * dtb_docr.costo_unt as CostoVendutoNetto,
	dtb_tipi.segno_val_scar *   (dtb_docr.importo_riga +
                       ABS((dtb_docr.qta_doc*dtb_docr.val_unt*dtb_docr.perc_gest/100)+(dtb_docr.qta_doc*dtb_docr.val_gest)) +
                       ABS((dtb_docr.importo_riga*dtb_docr.perc_promo/100)+(dtb_docr.qta_doc*dtb_docr.val_promo)) ) -
             (dtb_tipi.segno_qta_scar * dtb_docr.qta_doc * dtb_docr.costo_unt) as MargineLordo



from [VG_LINKED].salpar.[dbo].dtb_doct, [VG_LINKED].salpar.[dbo].dtb_tipi, [VG_LINKED].salpar.[dbo].dtb_docr, [VG_LINKED].salpar.[dbo].mtb_aart
where 
dtb_doct.cod_anag = dtb_docr.cod_anag and
dtb_doct.cod_dtip = dtb_docr.cod_dtip and
dtb_doct.data_doc = dtb_docr.data_doc and
dtb_doct.ser_doc = dtb_docr.ser_doc and
dtb_doct.num_doc = dtb_docr.num_doc and
dtb_docr.cod_mart = mtb_aart.cod_mart and
dtb_doct.cod_dtip = dtb_tipi.cod_dtip and dtb_doct.gestione = 'V' and
dtb_tipi.tipo_emissione = 'DIRETTA' and
dtb_doct.data_doc >= Dateadd( day ,-15,GETDATE ( )) and
dtb_tipi.flag_incl_stat = 'S'


