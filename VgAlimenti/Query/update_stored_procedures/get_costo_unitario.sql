USE [vgalimenti_NN_project]
GO
/****** Object:  StoredProcedure [dbo].[get_costo_unitario]    Script Date: 24/06/2021 18:54:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[get_costo_unitario]
	-- Add the parameters for the stored procedure here

AS


BEGIN

delete from costo_unitario
insert into costo_unitario
select cod_mart,avg(val_ult_car) 
from
(
select distinct cod_mart,val_ult_car
from vg_linked.vgalimenti.dbo.mtb_aart
union 
select distinct cod_mart,val_ult_car
from vg_linked.salpar.dbo.mtb_aart
) costi
group by cod_mart
END
