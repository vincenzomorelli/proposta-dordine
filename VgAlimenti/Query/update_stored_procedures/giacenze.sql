
CREATE TABLE giacenza(
	cod_mart varchar(15) not null,
	cod_mdep varchar (15),
	data_reg datetime not null,
	giacenza float
)
--necessaria per il calcolo giacenze da carichi e scarichi

--considero giacenza iniziale

with temp_giacenze ([cod_mart],[cod_mdep],[data_reg],[qta_acq],[qta_venduta]) as 
(
	Select cod_mart,
	cod_mdep,
	'2016/01/01' as data_reg,
	qta_iniz+qta_car-qta_Scar as qta_acq,
	0.00 as qta_venduta

	from mtb_sart

	Where anno = year('2015/01/01') and 
			qta_iniz+qta_car-qta_Scar > 0 and
			(qta_iniz+qta_car_costo) > 0 and cod_mdep in ('10','00017', '00018', '00019', '00020', '00021', '00022', '00023')	--and cod_mart='004028'
	UNION ALL

	Select cod_mart,cod_mdep,data_reg,qta_car,qta_scar
	from mtb_movi
	where data_reg>='2016-01-01' and cod_mdep in  ('10','00017', '00018', '00019', '00020', '00021', '00022', '00023')
	--and cod_mart='004028'
	--order by data_reg
	)
	--insert into giacenza
	select cod_mart,'10' as cod_mdep,data_reg,
		sum(sum(qta_acq-qta_venduta)) OVER (partition by  cod_mart order by  cod_mart, data_reg ) as giacenza
		From temp_giacenze 
		where data_reg >= '2016/01/01' 
		group by  cod_mart, data_reg
		order by cod_mdep,cod_mart,data_reg