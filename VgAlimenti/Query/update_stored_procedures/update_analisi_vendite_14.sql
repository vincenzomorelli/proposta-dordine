USE [vgalimenti_NN_project]
GO
/****** Object:  StoredProcedure [dbo].[update_analisi_vendite_14]    Script Date: 24/06/2021 18:52:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




ALTER PROCEDURE [dbo].[update_analisi_vendite_14] @periodo varchar(1), @newDepo varchar(1)

AS

BEGIN
	SET NOCOUNT ON;
 
	declare @data_back DATETIME, @codMdep varchar(5);

	 /*@periodo Periodo di popolamento dei dati
	 **G=Giornaliero
	 **S=Settimanale
	 **T=Totale
	 **@newDepo: 'S' Popola solo il nuovo deposito/'N' Tutti gli altri deposit
	 */ 

	SELECT @newDepo = ISNULL(@newDepo, 'N')
	set @data_back= CAST(getDate() as date);

	 SELECT @data_back =  case @periodo 
							WHEN 'G' THEN DATEADD(month, -1, @data_back)
							WHEN 'S' THEN DATEADD(month, -2, @data_back)
							WHEN 'T' THEN '2016/01/01'
							ELSE @data_back END;


	DECLARE crs_depo CURSOR LOCAL SCROLL FOR
	SELECT DISTINCT cod_mdep FROM tmp_depo WHERE new_depo = @newDepo 
	OPEN crs_depo 
	FETCH NEXT FROM crs_depo INTO @codMdep 

	WHILE @@FETCH_STATUS = 0 
		BEGIN

		 declare @rc int; --record count
  set @rc = 1;
 
	while @rc > 0
	begin
		DELETE TOP ( 1000 )
		FROM analisi_vendite WHERE data_reg >= @data_back and cod_mdep = @codMdep;

		set @rc = @@ROWCOUNT;
	end

;	
with tmpUnificazioneDepositi as (
		select case when codDeposito = '11' then '11' else '10' end as codDeposito,
	sum(case when sum(QuantitaVenduta) = 0.0 then 0 else 1 end) over ( Partition by codArticolo,codPtoVend order by codArticolo,codCliente,codPtoVend,dataDoc,sum(QuantitaVenduta) desc) as value_partition,
	dataDoc,
	min(data_ord) as data_ord,
	codArticolo,
	CodAgente,
	codCliente,
	codPtoVend,
	sum(QuantitaSostituita) as QuantitaSostituita,
	sum(QuantitaResa) as QuantitaResa,
	sum(ConfRese) as ConfRese ,
	sum(QuantitaVenduta) as QuantitaVenduta,
	sum(QuantitaOmaggi) as QuantitaOmaggi
	from OLAPVenduto
	where
	dataDoc>=DATEADD(MONTH, -3, @data_back)
	group by cod_dtip,dataDoc,codArticolo,CodAgente,codCliente,codPtoVend,codDeposito

	union all
	select case when codDeposito = '11' then '11' else '10' end as codDeposito,
	sum(case when sum(QuantitaOmaggi) = 0.0 then 0 else 1 end) over ( Partition by codArticolo,codPtoVend order by codArticolo,codCliente,codPtoVend,dataDoc,sum(QuantitaOmaggi) desc) as value_partition,
	dataDoc,
	min(data_ord) as data_ord,
	codArticolo,
	CodAgente,
	codCliente,
	codPtoVend,
	sum(QuantitaSostituita) as QuantitaSostituita,
	sum(QuantitaResa) as QuantitaResa,
	sum(ConfRese) as ConfRese ,
	sum(QuantitaVenduta) as QuantitaVenduta,
	sum(QuantitaOmaggi) as QuantitaOmaggi
	from OLAPVenduto_Salpar
	where
	dataDoc>=DATEADD(MONTH, -3, @data_back) and codCliente not in ('C0026', 'C1000', 'C5285', 'F1000') -- and codArticolo='138716' and data_ord between '2020-01-31' and '2020-02-06'
	group by cod_dtip,dataDoc,codArticolo,CodAgente,codCliente,codPtoVend,codDeposito
	 )
	  ,grouptmpUnificazioneDepositi as(
 select  codDeposito,value_partition,dataDoc,data_ord,codArticolo,CodAgente,codCliente,codPtoVend,
	sum(QuantitaSostituita) as QuantitaSostituita ,
	sum(QuantitaResa) as QuantitaResa,
	sum(ConfRese) as ConfRese,
	sum(QuantitaVenduta) as  QuantitaVenduta,
	sum(QuantitaOmaggi) as  QuantitaOmaggi
 from tmpUnificazioneDepositi
 group by codDeposito,value_partition,dataDoc,data_ord,codArticolo,CodAgente,codCliente,codPtoVend
 )
 
	,tmpSuResi as (
	select  '10' as codDeposito,
	dataDoc,
	data_ord,
	codArticolo,
	CodAgente,
	codCliente,
	codPtoVend,
	lead(sum(QuantitaSostituita),1,0) over(partition by codArticolo,codCliente,codPtoVend order by value_partition) as [QuantitaSostituita],
	lead(sum(QuantitaResa),1,0) over(partition by codArticolo,codCliente,codPtoVend order by value_partition)as [QuantitaResa],
	lead(sum(ConfRese),1,0) over(partition by codArticolo,codCliente,codPtoVend order by value_partition)as ConfRese,
	sum(QuantitaVenduta) + isnull(sum([QuantitaOmaggi]),0.0) as QuantitaVenduta,
	sum(QuantitaOmaggi) as QuantitaOmaggi
	from  grouptmpUnificazioneDepositi 
	group by codArticolo,CodAgente,codCliente,codPtoVend,value_partition,dataDoc ,data_ord
	--order by codArticolo,CodAgente,codCliente,codPtoVend,value_partition
	)
	--articoli venduti per mese per punto vendita
		,venditeArticoli  as (

		select distinct ga.data_doc as data_doc,
		ga.cod_mdep,
		cod_mart,
		ga.CodAgente,
		ga.codCliente,
		ga.codPtoVend
		from  artVendMese  avm inner join gg_rifornimento ga
		on ga.mese = avm.mese and ga.cod_mdep=avm.cod_mdep and ga.anno=avm.anno and ga.CodAgente =avm.CodAgente
		and ga.codCliente=avm.codCliente and ga.codPtoVend =avm.codPtoVend
		
		Union 
		select distinct data_reg,cod_mdep,cod_mart,codAgente,codCliente,codPtoVend
		from inevaso where data_reg>=DATEADD(MONTH, -3, @data_back) 
		
		)
	,venditeFinali as(
		
		select tfv.data_ord,data_doc as data_reg,
		va.cod_mdep,
		va.cod_mart,
		isnull(tfv.codAgente,va.CodAgente) as codAgente,
		va.codCliente,
		va.codPtoVend,
		isnull([QuantitaVenduta],0) as qta_venduta
	   ,isnull([QuantitaSostituita],0) as [QuantitaSostituita]
	   ,isnull(QuantitaResa,0) as QuantitaResa
	   ,isnull(ConfRese,0)as [ConfRese]
	   ,isnull([QuantitaOmaggi],0)as [QuantitaOmaggi]
		from venditeArticoli  va left join tmpSuResi tfv on va.cod_mdep=tfv.codDeposito and va.data_doc=tfv.dataDoc and va.cod_mart=tfv.codArticolo
		and  va.codCliente=tfv.codCliente and  va.codPtoVend=tfv.codPtoVend and va.codAgente=tfv.CodAgente
		where va.cod_mart is not null
		--order by cod_mart,codPtoVend,data_reg
		)
	,aggiuntaPromoPrezzo as (
		
		SELECT ab.cod_mart,
		ab.cod_mdep ,
		ab.data_ord,
		ab.data_reg,
		ab.codAgente,
		ab.codCliente,
		ab.codPtoVend,
		isnull(isNull(dest.cod_vlis,clie.cod_vlis),'NoDef') as cod_vlis,
		DATEPART( MONTH , ab.data_reg ) as mese,
		DATEPART( DAY , ab.data_reg ) as GGmese,
		DATEPART( WEEK ,ab.data_reg ) as settimana,
		DATEPART( WEEKDAY , ab.data_reg ) as GGsettimana,
		isnull(sum_promo,0) as sum_promo,
		isnull(avg_prezzo_vendita,0.0) as avg_prezzo_vendita,
		isnull(min_prezzo_vendita,0.0) as min_prezzo_vendita,
		isnull(max_prezzo_vendita,0.0) as max_prezzo_vendita,
		isnull(avg_variazione_prezzo,0.0) as avg_variazione_prezzo,
		isnull(min_variazione_prezzo,0.0) as min_variazione_prezzo,
		isnull(max_variazione_prezzo,0.0) as max_variazione_prezzo,
		isnull(stdev_variazione_prezzo,0.0) as stdev_variazione_prezzo,
		(isnull(qta_venduta,0.0) - isnull([QuantitaSostituita],0.0) - isnull(QuantitaResa,0.0) + isnull(inevaso,0.0)) as qta_venduta
	   ,isnull([QuantitaSostituita],0.0) as [QuantitaSostituita]
	   ,isnull(QuantitaResa,0.0) as QuantitaResa
	   ,isnull(ConfRese,0.0)as [ConfRese]
	   ,isnull([QuantitaOmaggi],0.0)as [QuantitaOmaggi],
		sum(qta_venduta) OVER (Partition by ab.cod_mdep,ab.cod_mart,ab.data_reg order by ab.cod_mdep,ab.cod_mart,ab.data_reg) as qta_venduta_giornaliera,
		avg(isnull(avg_prezzo_vendita,0.0)) OVER (Partition by ab.cod_mdep,ab.cod_mart,ab.data_reg order by ab.cod_mdep,ab.cod_mart,ab.data_reg) as prezzo_giornaliero,
	   inevaso,
	   importo

		FROM   venditeFinali ab left outer join prezzo_vendita_articolo pva 
								on ab.cod_mart=pva.cod_mart and ab.data_reg=pva.data_reg 
								left join vtb_clie clie on codCliente=clie.cod_anag 
								left join vtb_dest dest on codCliente= dest.cod_anag and codPtoVend=cod_vdes
								left join inevaso on ab.cod_mart=inevaso.cod_mart and ab.data_reg=inevaso.data_reg and ab.cod_mdep=inevaso.cod_mdep 
										and ab.codCliente=inevaso.codCliente and ab.codAgente=inevaso.codAgente and ab.codPtoVend =inevaso.codPtoVend
		WHERE pva.data_reg>=DATEADD(MONTH, -3, @data_back)
		
		
		)
 , venditeTotali as (
	SELECT ab.cod_mart,
		ab.cod_mdep ,
		isnull(ab.data_ord,ab.data_reg) as data_ord,
		ab.data_reg,
		mese,
		GGmese,
		settimana,
		GGsettimana,
		case when data_ricorrenza is null then  0 else 1 end as ricorrenza,
		isnull(ab.codAgente,'nodef') as codAgente ,
		isnull(ab.codCliente,'nodef') as codCliente,
		isnull(ab.codPtoVend,'nodef' )as codPtoVend,
		[peso],
		[descr_mgrp],
		[descr_msgr],
		[descr_msfa],
		qta_cnf,
		gg_scad_partita,
		promoNoPromo,
		sum_promo,
		isnull(avg(qta_venduta) over (partition by  DATEPART( WEEKDAY ,ab.data_reg ),ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg ROWS BETWEEN 5 PRECEDING AND 1 PRECEDING),isnull(qta_venduta,0))  as QtaMediaVendGiorn_ptoVend,
		isnull(avg(avg_prezzo_vendita) over (partition by  DATEPART( WEEKDAY ,ab.data_reg ),ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg ROWS BETWEEN 5 PRECEDING AND 1 PRECEDING),avg_prezzo_vendita) as PrezzoMedioVendGiorn,
		isnull(avg(prezzo_vendita) over (partition by  DATEPART( WEEKDAY ,ab.data_reg ),ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg ROWS BETWEEN 5 PRECEDING AND 1 PRECEDING),avg_prezzo_vendita) as PrezzoMedioVendGiorn_ptoVend,
		LAG(qta_venduta,1,0) OVER (Partition by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg,mese,GGmese,GGsettimana) as day1_ptoVend,
		LAG(qta_venduta,2,0) OVER (Partition by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg,mese,GGmese,GGsettimana) as day2_ptoVend,
		LAG(qta_venduta,3,0) OVER (Partition by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg,mese,GGmese,GGsettimana) as day3_ptoVend,
		LAG(qta_venduta,4,0) OVER (Partition by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg,mese,GGmese,GGsettimana) as day4_ptoVend,
		LAG(qta_venduta,5,0) OVER (Partition by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg,mese,GGmese,GGsettimana) as day5_ptoVend,
		LAG(qta_venduta,6,0) OVER (Partition by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg,mese,GGmese,GGsettimana) as day6_ptoVend,
		LAG(qta_venduta,7,0) OVER (Partition by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg,mese,GGmese,GGsettimana) as day7_ptoVend,
		prezzo_vendita,
		avg_prezzo_vendita,
		min_prezzo_vendita,
		max_prezzo_vendita,
		variazione_prezzo,
		avg_variazione_prezzo,
		min_variazione_prezzo,
		max_variazione_prezzo,
		stdev_variazione_prezzo,
		isnull(STDEV(qta_venduta) OVER(Partition by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg,mese,GGmese,GGsettimana ROWS BETWEEN 18 PRECEDING AND 1 PRECEDING),0) as dev_std_30,
		isnull(AVG(qta_venduta) OVER(Partition by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg,mese,GGmese,GGsettimana ROWS BETWEEN 18 PRECEDING AND 1 PRECEDING),0) as media_30,
		isnull(AVG(qta_venduta) OVER(Partition by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg,mese,GGmese,GGsettimana ROWS BETWEEN 9 PRECEDING AND 1 PRECEDING),0) as media_15,
		isnull(AVG(qta_venduta) OVER(Partition by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg,mese,GGmese,GGsettimana ROWS BETWEEN 6 PRECEDING AND 1 PRECEDING),0) as media_7
	   ,[QuantitaSostituita]
	   ,QuantitaResa
	   ,[ConfRese]
	   ,case when [QuantitaOmaggi] <> qta_venduta then QuantitaOmaggi else 0 end as QuantitaOmaggi
	   ,isnull(inevaso,0) as inevaso,
	    isnull(importo,0) as importo_inevaso
		,qta_venduta ,
		dateadd(day,cms.lag_consegna_medio,ab.data_reg) as lag_consegna_medio
	  FROM   aggiuntaPromoPrezzo ab 
						
							left join vgalimenti_NN_project.dbo.ricorrenze on ab.data_reg=data_ricorrenza 
							left join vgalimenti_NN_project.dbo.articoli_movimentati art on ab.cod_mart=art.cod_mart 
							left join prezzo_vendita_articolo_listino pval on ab.cod_mart=pval.cod_mart and ab.data_reg=pval.data_reg and ab.cod_vlis=pval.cod_vlis
							left join consegne_medie_stimate cms on ab.cod_mart=cms.cod_mart and ab.cod_mdep=cms.cod_mdep and ab.codCliente = cms.codCliente and ab.codPtoVend=cms.codPtoVend

	  WHERE ab.cod_mdep in ('10') 

	 -- order by ab.cod_mdep,ab.cod_mart,codPtoVend,ab.data_reg 
				
				
		)	
			
		

			INSERT INTO analisi_vendite
			SELECT *
			FROM venditeTotali
			WHERE data_reg >= @data_back 
			order by cod_mart, data_reg desc
		FETCH NEXT FROM crs_depo INTO @codMdep
	END 
	CLOSE crs_depo 
	DEALLOCATE crs_depo 
END

