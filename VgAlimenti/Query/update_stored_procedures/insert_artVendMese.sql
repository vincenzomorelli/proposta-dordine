USE [vgalimenti_NN_project]
GO
/****** Object:  StoredProcedure [dbo].[insert_artVendMese]    Script Date: 24/06/2021 18:54:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[insert_artVendMese]
	
AS

 
declare @data_back DATETIME;
set @data_back= dateadd(day,-1,convert(date, getdate()));

with tmpUnificazioneDepositi as (
		select case when codDeposito = '11' then '11' else '10' end as codDeposito,
	sum(case when sum(QuantitaVenduta) = 0.0 then 0 else 1 end) over ( Partition by codArticolo,codPtoVend order by codArticolo,codCliente,codPtoVend,dataDoc,sum(QuantitaVenduta) desc) as value_partition,
	dataDoc,
	min(data_ord) as data_ord,
	codArticolo,
	CodAgente,
	codCliente,
	codPtoVend,
	sum(QuantitaSostituita) as QuantitaSostituita,
	sum(QuantitaResa) as QuantitaResa,
	sum(ConfRese) as ConfRese ,
	sum(QuantitaVenduta) as QuantitaVenduta,
	sum(QuantitaOmaggi) as QuantitaOmaggi
	from OLAPVenduto
	where
	dataDoc>=DATEADD(MONTH, -3, @data_back)
	group by cod_dtip,dataDoc,codArticolo,CodAgente,codCliente,codPtoVend,codDeposito

	union all
	select case when codDeposito = '11' then '11' else '10' end as codDeposito,
	sum(case when sum(QuantitaOmaggi) = 0.0 then 0 else 1 end) over ( Partition by codArticolo,codPtoVend order by codArticolo,codCliente,codPtoVend,dataDoc,sum(QuantitaOmaggi) desc) as value_partition,
	dataDoc,
	min(data_ord) as data_ord,
	codArticolo,
	CodAgente,
	codCliente,
	codPtoVend,
	sum(QuantitaSostituita) as QuantitaSostituita,
	sum(QuantitaResa) as QuantitaResa,
	sum(ConfRese) as ConfRese ,
	sum(QuantitaVenduta) as QuantitaVenduta,
	sum(QuantitaOmaggi) as QuantitaOmaggi
	from OLAPVenduto_Salpar
	where
	dataDoc>=DATEADD(MONTH, -3, @data_back) and codCliente not in ('C0026', 'C1000', 'C5285', 'F1000') -- and codArticolo='138716' and data_ord between '2020-01-31' and '2020-02-06'
	group by cod_dtip,dataDoc,codArticolo,CodAgente,codCliente,codPtoVend,codDeposito
	 )
	  ,grouptmpUnificazioneDepositi as(
 select  codDeposito,value_partition,dataDoc,min(data_ord) as data_ord,codArticolo,CodAgente,codCliente,codPtoVend,
	sum(QuantitaSostituita) as QuantitaSostituita ,
	sum(QuantitaResa) as QuantitaResa,
	sum(ConfRese) as ConfRese,
	sum(QuantitaVenduta) as  QuantitaVenduta,
	sum(QuantitaOmaggi) as  QuantitaOmaggi
 from tmpUnificazioneDepositi
 group by codDeposito,value_partition,dataDoc,codArticolo,CodAgente,codCliente,codPtoVend
 )
 
	,tmpSuResi as (
	select  '10' as codDeposito,
	dataDoc,
	min(data_ord) as data_ord,
	codArticolo,
	CodAgente,
	codCliente,
	codPtoVend,
	lead(sum(QuantitaSostituita),1,0) over(partition by codArticolo,codCliente,codPtoVend order by value_partition) as [QuantitaSostituita],
	lead(sum(QuantitaResa),1,0) over(partition by codArticolo,codCliente,codPtoVend order by value_partition)as [QuantitaResa],
	lead(sum(ConfRese),1,0) over(partition by codArticolo,codCliente,codPtoVend order by value_partition)as ConfRese,
	sum(QuantitaVenduta) + isnull(sum([QuantitaOmaggi]),0.0) as QuantitaVenduta,
	sum(QuantitaOmaggi) as QuantitaOmaggi
	from  grouptmpUnificazioneDepositi 
	group by codArticolo,CodAgente,codCliente,codPtoVend,value_partition,dataDoc 
	--order by codArticolo,CodAgente,codCliente,codPtoVend,value_partition
	)
	--articoli venduti per mese per punto vendita
	insert into artVendMese
		
		select distinct 
		DATEPART(MONTH,dataDoc) as mese,DATEPART(YEAR,dataDoc) as anno,codArticolo as cod_mart,
		codDeposito as cod_mdep,CodAgente,codCliente,codPtoVend
		from tmpSuResi
		where (QuantitaVenduta>0 or QuantitaOmaggi>0) 
		
		Union
		
		select distinct 
		DATEPART(MONTH,dateadd(month,-1,dataDoc)) as mese,DATEPART(YEAR,dateadd(month,-1,dataDoc)) as anno,codArticolo as cod_mart,
		codDeposito as cod_mdep,CodAgente,codCliente,codPtoVend
		from tmpSuResi
		where (QuantitaVenduta>0 or QuantitaOmaggi>0) 
														and dataDoc>=DATEADD(MONTH, -2, @data_back)

		order by mese,anno

	



