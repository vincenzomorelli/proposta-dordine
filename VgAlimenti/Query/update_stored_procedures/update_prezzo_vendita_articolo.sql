USE [vgalimenti_NN_project]
GO
/****** Object:  StoredProcedure [dbo].[update_prezzo_vendita_articolo_listino]    Script Date: 13/11/2020 09:29:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[update_prezzo_vendita_articolo_listino] (@periodo varchar(1), @newDepo varchar(1)) AS 
BEGIN
SET NOCOUNT ON;

DECLARE @dataIniz datetime, @codMdep varchar(5), @codVlis varchar(5), @dataFine datetime

SET @dataIniz = CAST(getDAte() AS date)
 

SELECT @dataIniz =  case @periodo 
                    WHEN 'S' THEN DATEADD(WEEK, -2, @dataIniz)
                    WHEN 'T' THEN'2016/01/01'
                    ELSE @dataIniz END;

 

SELECT @dataFine =  DATEADD(week, 2, Cast(getDAte() as date));

 


  declare @rc int; --record count
  set @rc = 1;
 
    while @rc > 0
    begin
            DELETE 
            FROM prezzo_vendita_articolo_listino
            WHERE data_reg >= @dataIniz

 


        set @rc = @@ROWCOUNT;
    end

	
  set @rc = 1;
 
    while @rc > 0
    begin
            DELETE 
            FROM prezzo_vendita_articolo
            WHERE data_reg >= @dataIniz

 
        set @rc = @@ROWCOUNT;
    end
    

 

;        while @dataIniz <= @dataFine
        begin

 

            
            insert into prezzo_vendita_articolo_listino
            SELECT cod_mart, 
			@dataIniz as data_iniz,
            lisv.cod_vlis,
            PromoNoPromo, 
            lisv.prz_vend,
            VariazionePrezzo
            FROM (
                select lisv.cod_vlis,lisv.cod_mart,
                isNull(Promov.prz_vend, lisv.prz_vend) as prz_vend,
                case when PromoV.prz_vend is null then 0 else case when Promov.prz_vend > lisv.prz_vend then 0 else 1 end end as PromoNoPromo,
                case when PromoV.prz_vend is null then 0 else case when Promov.prz_vend > lisv.prz_vend then 0 else  case when lisv.prz_vend>0 
                then (lisv.prz_vend-Promov.prz_vend)/lisv.prz_vend else 0 end end end as VariazionePrezzo
                from  dbo.getListinoVendita(@dataIniz, null, null ) lisv 
                        left outer join dbo.getPromozioneVendita(@dataIniz, @dataIniz, null,null,null) promoV
                         ON lisv.cod_vlis = promov.cod_vlis and lisv.cod_mart = promoV.cod_mart
             )lisv;


			  
			insert into prezzo_vendita_articolo
			SELECT cod_mart, @dataIniz, 
			sum(PromoNoPromo) as sum_promo,
			avg(lisv.prezzo_vendita) as avg_prezzo_vendita,
			min(lisv.prezzo_vendita) as min_prezzo_vendita,
			max(lisv.prezzo_vendita) as max_prezzo_vendita,
			avg(Variazione_prezzo) as avg_variazione_prezzo ,
			min(Variazione_prezzo) as min_variazione_prezzo,
			max(Variazione_prezzo) as max_variazione_prezzo,
			stdev(Variazione_prezzo) as stdev_variazione_prezzo
			FROM prezzo_vendita_articolo_listino lisv
			where lisv.data_reg = @dataIniz
			 group by cod_mart

 

            set @dataIniz = DATEADD(day, 1, @dataIniz )
        end

 

 

END