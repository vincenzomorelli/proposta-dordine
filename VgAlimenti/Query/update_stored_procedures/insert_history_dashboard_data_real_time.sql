USE [vgalimenti_NN_project]
GO
/****** Object:  StoredProcedure [dbo].[insert_history_dashboard_data_real_time]    Script Date: 24/06/2021 18:45:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[insert_history_dashboard_data_real_time] 

AS



with final_table as (
select
		p.cod_mart,
		p.data_predizione,
		p.finestra,
		isnull(p.predizione,0) as predizione,
		ic.impegnato,
		ic.in_consegna,
		ic.qta_esistente,
		ic.disponibilita,
		ic.qta_esistente + ic.in_consegna -isnull(p.predizione,0) as fabbisogno,
		errore_sottostima,
		g.giacenza_media,
		qta_cnf
from temp_final_table p
left join impegnato_consegna_provvisorio ic on ic.cod_mart = p.cod_mart and ic.data_predizione = p.data_predizione and ic.finestra = p.finestra
left join giacenza g on g.cod_mart = p.cod_mart 
left join errori_medi e on e.cod_mart= p.cod_mart and e.finestra=p.finestra
left join articoli_movimentati am on p.cod_mart=am.cod_mart
where ic.data_predizione= convert(date, getdate()) and p.data_predizione = convert(date, getdate())
)

, adjusted_rapporto as (
select
		cod_mart,
		data_predizione,
		finestra,
		predizione,
		impegnato,
		in_consegna,
		qta_esistente,
		disponibilita,
		fabbisogno,
		isnull(giacenza_media,qta_esistente) giacenza_media,
		qta_cnf,
		errore_sottostima,
		case when cod_mart like 'SNB%' then 0.85
		when cod_mart like 'ARR%' then 0.60
		when cod_mart = '155506' then 0.60
		when cod_mart = '847311' then 0.60 
		when cod_mart like 'BMU%' then 0.85 
		when cod_mart = '135006' then 0.50 
		when cod_mart = '831005' then 0.30 
		when cod_mart like '831%' then 0.20 
		when cod_mart like '8332%' then 0.20 
		when cod_mart like '8320%' then 0.20 
		when cod_mart like '8340%' then 0.20 
		when cod_mart = '752142' then 0.10 
		when cod_mart not like 'SNB%' and cod_mart not like '831%' and giacenza_media > 0 and errore_sottostima > 0 and errore_sottostima / giacenza_media < 0.30 then 0.30 
		else 0.40 end as rapporto_errore_giacenza_adjusted
from final_table
)
		
insert into history_dashboard_data_real_time
select 
		cod_mart,
		data_predizione,
		DATEADD(day, finestra, data_predizione) as data_copertura,
		finestra,
		round(impegnato/qta_cnf,2) as impegnato_cnf,
		round(in_consegna/qta_cnf,2) as in_consegna_cnf,
		round(predizione/qta_cnf,2) as predizione,
		isnull(round(errore_sottostima/qta_cnf,2),0) as errore_max_cnf,
		round(giacenza_media/qta_cnf,2) as giacenza_media_cnf,
		cast(rapporto_errore_giacenza_adjusted as numeric(2,2)) as rapporto_errore_giacenza,
		round(qta_esistente/qta_cnf,2) as qta_esistente_cnf,
		round(disponibilita/qta_cnf,2) as disponibilita_cnf,
		round(fabbisogno/qta_cnf,2) as fabbisogno_cnf,
		giacenza_media*(1-((1-rapporto_errore_giacenza_adjusted)- 0.15))/qta_cnf as soglia,
		isnull(case when giacenza_media*(rapporto_errore_giacenza_adjusted- 0.05) - fabbisogno < 0 then 0 
			 else round(((isnull(giacenza_media,round(disponibilita/qta_cnf,2))*(rapporto_errore_giacenza_adjusted) - fabbisogno)/qta_cnf),2) end,predizione) as proposta_ordine
from adjusted_rapporto 


