USE [vgalimenti_NN_project]
GO
/****** Object:  StoredProcedure [dbo].[insert_gg_rifornimento]    Script Date: 24/06/2021 18:54:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[insert_gg_rifornimento]
	
AS

 
declare @data_back DATETIME;
set @data_back= dateadd(day,-1,convert(date, getdate()));

with tmpUnificazioneDepositi as (
		select case when codDeposito = '11' then '11' else '10' end as codDeposito,
	sum(case when sum(QuantitaVenduta) = 0.0 then 0 else 1 end) over ( Partition by codArticolo,codPtoVend order by codArticolo,codCliente,codPtoVend,dataDoc,sum(QuantitaVenduta) desc) as value_partition,
	dataDoc,
	min(data_ord) as data_ord,
	codArticolo,
	CodAgente,
	codCliente,
	codPtoVend,
	sum(QuantitaSostituita) as QuantitaSostituita,
	sum(QuantitaResa) as QuantitaResa,
	sum(ConfRese) as ConfRese ,
	sum(QuantitaVenduta) as QuantitaVenduta,
	sum(QuantitaOmaggi) as QuantitaOmaggi
	from OLAPVenduto
	where
	dataDoc>=DATEADD(MONTH, -3, @data_back)
	group by cod_dtip,dataDoc,codArticolo,CodAgente,codCliente,codPtoVend,codDeposito

	union all
	select case when codDeposito = '11' then '11' else '10' end as codDeposito,
	sum(case when sum(QuantitaOmaggi) = 0.0 then 0 else 1 end) over ( Partition by codArticolo,codPtoVend order by codArticolo,codCliente,codPtoVend,dataDoc,sum(QuantitaOmaggi) desc) as value_partition,
	dataDoc,
	min(data_ord) as data_ord,
	codArticolo,
	CodAgente,
	codCliente,
	codPtoVend,
	sum(QuantitaSostituita) as QuantitaSostituita,
	sum(QuantitaResa) as QuantitaResa,
	sum(ConfRese) as ConfRese ,
	sum(QuantitaVenduta) as QuantitaVenduta,
	sum(QuantitaOmaggi) as QuantitaOmaggi
	from OLAPVenduto_Salpar
	where
	dataDoc>=DATEADD(MONTH, -3, @data_back) and codCliente not in ('C0026', 'C1000', 'C5285', 'F1000') -- and codArticolo='138716' and data_ord between '2020-01-31' and '2020-02-06'
	group by cod_dtip,dataDoc,codArticolo,CodAgente,codCliente,codPtoVend,codDeposito
	 )
	  ,grouptmpUnificazioneDepositi as(
 select  codDeposito,value_partition,dataDoc,min(data_ord) as data_ord,codArticolo,CodAgente,codCliente,codPtoVend,
	sum(QuantitaSostituita) as QuantitaSostituita ,
	sum(QuantitaResa) as QuantitaResa,
	sum(ConfRese) as ConfRese,
	sum(QuantitaVenduta) as  QuantitaVenduta,
	sum(QuantitaOmaggi) as  QuantitaOmaggi
 from tmpUnificazioneDepositi
 group by codDeposito,value_partition,dataDoc,codArticolo,CodAgente,codCliente,codPtoVend
 )
 
	,tmpSuResi as (
	select  '10' as codDeposito,
	dataDoc,
	min(data_ord) as data_ord,
	codArticolo,
	CodAgente,
	codCliente,
	codPtoVend,
	lead(sum(QuantitaSostituita),1,0) over(partition by codArticolo,codCliente,codPtoVend order by value_partition) as [QuantitaSostituita],
	lead(sum(QuantitaResa),1,0) over(partition by codArticolo,codCliente,codPtoVend order by value_partition)as [QuantitaResa],
	lead(sum(ConfRese),1,0) over(partition by codArticolo,codCliente,codPtoVend order by value_partition)as ConfRese,
	sum(QuantitaVenduta) + isnull(sum([QuantitaOmaggi]),0.0) as QuantitaVenduta,
	sum(QuantitaOmaggi) as QuantitaOmaggi
	from  grouptmpUnificazioneDepositi 
	group by codArticolo,CodAgente,codCliente,codPtoVend,value_partition,dataDoc 
	--order by codArticolo,CodAgente,codCliente,codPtoVend,value_partition
	)
	--articoli venduti per mese per punto vendita
	 ,artVendMese as(
		
		select distinct 
		DATEPART(MONTH,dataDoc) as mese,DATEPART(YEAR,dataDoc) as anno,codArticolo as cod_mart,
		codDeposito as cod_mdep,CodAgente,codCliente,codPtoVend
		from tmpSuResi
		where (QuantitaVenduta>0 or QuantitaOmaggi>0) 
		
		),
	giorni_td as(
		Select distinct
		DATEPART(weekday,dataDoc)as giorno,
		CodAgente,
		codCliente,
		codPtoVend

		from tmpSuResi
		where dataDoc>=DATEADD(MONTH, -1, @data_back)
	
	)
	--giorni in cui il punto vendita è stato rifornito
	,gg_riforniment as( 
		
		select distinct dataDoc as data_doc,
		codDeposito as cod_mdep,
		CodAgente,
		codCliente,
		codPtoVend,
		DATEPART(MONTH,dataDoc) as mese,
		DATEPART(YEAR,dataDoc) as anno

		from tmpSuResi
		where (QuantitaVenduta>0 or QuantitaOmaggi>0) 

		union
		SELECT v.d as data_doc,
		'10' as cod_mdep,
		[cod_vage] as codAgente,
		[cod_anag] as codCliente,
		[cod_vdes] as codPtoVend,
		mese,
		anno
		FROM [dbo].[vtb_ord_giror] a
		left join
	 ( select tabDate.d,DATEPART(weekday,tabDate.d) as giorno,
						DATEPART(MONTH,DATEADD(MONTH,-1,tabDate.d))  as mese,
						DATEPART(YEAR,DATEADD(MONTH,-1,tabDate.d))as anno
					FROM vgalimenti.dbo.getTableDate(Cast(@data_back  as date), DateAdd(day, 15, Cast(@data_back  as date))) tabDate 
		) v
					on a.giorno=v.giorno
	  where cod_vage in (select distinct codAgente
		from  tmpSuResi
		)

		union 

		SELECT v.d as data_doc,
		'10' as cod_mdep,
		 codAgente,
		 codCliente,
		 codPtoVend,
		 mese,
		 anno
		FROM giorni_td a
		left join
	 ( select tabDate.d,DATEPART(weekday,tabDate.d) as giorno,
						DATEPART(MONTH,DATEADD(MONTH,-1,tabDate.d))  as mese,
						DATEPART(YEAR,DATEADD(MONTH,-1,tabDate.d))as anno
					FROM vgalimenti.dbo.getTableDate(Cast(@data_back  as date), DateAdd(day, 15, Cast(@data_back  as date))) tabDate 
		) v
					on a.giorno=v.giorno

		
	 

		)
	
	INSERT INTO gg_rifornimento
			SELECT *
			FROM gg_riforniment


