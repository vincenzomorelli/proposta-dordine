USE [vgalimenti_NN_project]
GO
/****** Object:  StoredProcedure [dbo].[delete_artVendMese_gg_rifornimento]    Script Date: 13/11/2020 09:38:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[delete_artVendMese_gg_rifornimento] 

AS
BEGIN
declare @data_back DATETIME;
set @data_back= dateadd(day,-1,convert(date, getdate()));

delete 
from gg_rifornimento 
where data_doc>=DATEADD(MONTH, -3, @data_back)

delete
from artVendMese
where (mese>=datepart(month,DATEADD(MONTH, -3, @data_back)) and anno = datepart(year,DATEADD(MONTH, -3, @data_back))) or
	(anno>datepart(year,DATEADD(MONTH, -3, @data_back)))
		

END
