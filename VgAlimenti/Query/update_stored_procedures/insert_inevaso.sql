USE [vgalimenti_NN_project]
GO
/****** Object:  StoredProcedure [dbo].[insert_inevaso]    Script Date: 24/06/2021 18:52:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[insert_inevaso]
	
AS
	declare @data_back DATETIME;
	set @data_back= dateadd(week,-2,CAST(getDate() as date));

delete from inevaso where data_reg>=@data_back

insert into inevaso
select data_ord as data_reg,cod_mart,cod_mdep,codAgente,codCliente,codPtoVend, SUM(qta_ord-qta_evasa) as inevaso,
SUM(importo_riga) as importo
from
(
select '10' as cod_mdep,
	dtb_ordt.cod_vage as codAgente,
	dtb_ordr.data_ord, dtb_ordt.cod_anag as codCliente, 
	dtb_ordt.cod_vdes as codPtoVend, 
	dtb_ordr.cod_mart, dtb_ordr.descrizione,
	dtb_ordr.unt_ord,
	dtb_ordr.qta_ord, 
	dtb_ordr.qta_evasa,
	dtb_ordr.qta_ord * (dtb_ordr.val_unt * (1-sconto5/100)* (1-sconto6/100)* (1-sconto7/100)* (1-sconto8/100)) as Importo_riga  ,
	dtb_ordr.cod_promo,
	dtb_ordr.flag_evaso
from dtb_ordt, dtb_ordr,mtb_aart
where dtb_ordt.gestione = dtb_ordr.gestione and
dtb_ordt.data_ord = dtb_ordr.data_ord and
dtb_ordt.num_ord = dtb_ordr.num_ord and
dtb_ordr.cod_mart = mtb_aart.cod_mart and
dtb_ordt.gestione = 'V' and
dtb_ordt.data_ord >= @data_back
and dtb_ordt.flag_annulla = 'N' 
and qta_ord > qta_evasa
and flag_evaso <> 'I' 

union all

select '10' as cod_mdep,
	dtb_ordt.cod_vage as codAgente,
	dtb_ordr.data_ord, dtb_ordt.cod_anag as codCliente, 
	dtb_ordt.cod_vdes as codPtoVend, 
	dtb_ordr.cod_mart, dtb_ordr.descrizione,
	dtb_ordr.unt_ord,
	dtb_ordr.qta_ord, 
	dtb_ordr.qta_evasa,
	dtb_ordr.qta_ord * (dtb_ordr.val_unt * (1-sconto5/100)* (1-sconto6/100)* (1-sconto7/100)* (1-sconto8/100)) as Importo_riga  ,
	dtb_ordr.cod_promo,
	dtb_ordr.flag_evaso
from vg_linked.salpar.dbo.dtb_ordt, vg_linked.salpar.dbo.dtb_ordr,vg_linked.salpar.dbo.mtb_aart
where dtb_ordt.gestione = dtb_ordr.gestione and
dtb_ordt.data_ord = dtb_ordr.data_ord and
dtb_ordt.num_ord = dtb_ordr.num_ord and
dtb_ordr.cod_mart = mtb_aart.cod_mart and
dtb_ordt.gestione = 'V' and
dtb_ordt.data_ord >=@data_back
and dtb_ordt.flag_annulla = 'N' 
and qta_ord > qta_evasa
and flag_evaso <> 'I' 
)q
group by data_ord,cod_mart,cod_mdep,codAgente,codCliente,codPtoVend