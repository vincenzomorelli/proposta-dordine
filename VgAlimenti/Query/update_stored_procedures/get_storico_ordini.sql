USE [vgalimenti_NN_project]
GO
/****** Object:  StoredProcedure [dbo].[get_storico_ordini]    Script Date: 24/06/2021 18:53:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[get_storico_ordini]
AS

declare @data date;
set @data = dateadd(month,-2,cast(getdate() as date));

with somma_ordini_vg as (
    select data_ord,cod_mart,sum(qta_evasa) as qta_evasa, sum(qta_ord) qta_ord,data_cons
    from vg_linked.vgalimenti.dbo.dtb_ordr where data_ord>=@data  and gestione='A'
    group by data_ord, cod_mart,data_cons
    )
, somma_documenti_vg as (
select data_ord,data_doc,sum(qta_doc) qta_doc,cod_mart
from vg_linked.vgalimenti.dbo.dtb_docr
    join vg_linked.vgalimenti.dbo.dtb_tipi on dtb_docr.cod_dtip=dtb_tipi.cod_dtip
where gestione='A'  and data_doc>=@data  and tipo_emissione='DIRETTA'
group by data_ord,data_doc,cod_mart
),somma_ordini_salpar as (
    select data_ord,cod_mart,sum(qta_evasa) as qta_evasa, sum(qta_ord) qta_ord,data_cons
    from vg_linked.salpar.dbo.dtb_ordr where data_ord>=@data  and gestione='A'
    group by data_ord, cod_mart,data_cons
    )
, somma_documenti_salpar as (
select data_ord,data_doc,sum(qta_doc) qta_doc,cod_mart
from vg_linked.salpar.dbo.dtb_docr
    join vg_linked.salpar.dbo.dtb_tipi on dtb_docr.cod_dtip=dtb_tipi.cod_dtip
where gestione='A'  and data_doc>=@data  and tipo_emissione='DIRETTA'
group by data_ord,data_doc,cod_mart
)
insert into storico_ordini
select  somma_ordini_vg.cod_mart
,somma_ordini_vg.data_ord
     ,case when qta_evasa>0 and somma_documenti_vg.data_doc is null  then somma_ordini_vg.data_ord else somma_documenti_vg.data_doc end as data_doc
     , qta_ord
     ,qta_evasa
     , case when data_doc is null  then 0 else qta_doc end as qta_doc
     , case when qta_ord - isnull(qta_evasa,0) = 0
         then 'OK'
      when  qta_ord - isnull(qta_evasa,0) < 0
          then 'SUPERIORE'
      else
          case when data_doc is null then  'NON CONSEGNATO'
          else 'CONSEGNA PARZIALE'
          end
         end as flag_consegna
    ,   qta_ord - isnull(qta_doc,0) as qta_diff
    , getdate() as ultimo_aggiornamento

from somma_ordini_vg
    left join somma_documenti_vg on somma_ordini_vg.cod_mart=somma_documenti_vg.cod_mart
                                and somma_ordini_vg.data_ord=somma_documenti_vg.data_ord
where somma_ordini_vg.cod_mart is not null
union all
select  somma_ordini_salpar.cod_mart,somma_ordini_salpar.data_ord
     ,case when qta_evasa>0 and somma_documenti_salpar.data_doc is null  then somma_ordini_salpar.data_ord else somma_documenti_salpar.data_doc end as data_doc, qta_ord
     ,qta_evasa
     , case when data_doc is null  then 0 else qta_doc end as qta_doc
     , case when qta_ord - isnull(qta_evasa,0) = 0
         then 'OK'
      when  qta_ord - isnull(qta_evasa,0) < 0
          then 'SUPERIORE'
      else
          case when data_doc is null then  'NON CONSEGNATO'
          else 'CONSEGNA PARZIALE'
          end
         end as flag_consegna
    ,   qta_ord - isnull(qta_doc,0) as qta_diff
    , getdate() as ultimo_aggiornamento

from somma_ordini_salpar
    left join somma_documenti_salpar on somma_ordini_salpar.cod_mart=somma_documenti_salpar.cod_mart
                                and somma_ordini_salpar.data_ord=somma_documenti_salpar.data_ord
where somma_ordini_salpar.cod_mart is not null