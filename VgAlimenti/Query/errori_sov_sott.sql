with tmp as (
select a.cod_mart,data_predizione,data_copertura,finestra,predizione*qta_cnf as predizione,
sum(qta_venduta) over (partition by a.cod_mart,data_predizione order by data_copertura rows unbounded preceding) as somma_finestra,
predizione*qta_cnf -sum(qta_venduta) over (partition by a.cod_mart,data_predizione order by data_copertura rows unbounded preceding) as differenza
from history_dashboard_data a 
join (
		select sum(qta_venduta) as qta_venduta,qta_cnf,cod_mart,data_reg 
		from analisi_vendite 
		--where cod_mart='831001' 
		group by cod_mart,data_reg,qta_cnf
	) b on a.cod_mart=b.cod_mart and a.data_copertura=b.data_reg
where finestra>1 and ((data_predizione>'2020-11-16' and data_predizione<'2020-12-01') or data_predizione>'2020-12-08') and data_reg<'2021-01-08'

)
insert into errori_medi
select tmp.cod_mart,finestra,abs(min(differenza)) errore_di_sottostima,abs(max(differenza)) as errore_di_sovrastima 
from tmp 
group by tmp.cod_mart,finestra
order by tmp.cod_mart, finestra 