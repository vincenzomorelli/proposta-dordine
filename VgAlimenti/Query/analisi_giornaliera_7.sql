/****** Script for SelectTopNRows command from SSMS  ******/
set datefirst 1;
with tmpUnificazioneDepositi as (
	select case when codDeposito = '11' then '11' else '10' end as codDeposito,
	sum(case when sum(QuantitaVenduta) = 0.0 then 0 else 1 end) over ( Partition by codArticolo,codPtoVend order by codArticolo,codCliente,codPtoVend,dataDoc,sum(QuantitaVenduta) desc) as value_partition,
	dataDoc,
	min(data_ord) as data_ord,
	codArticolo,
	CodAgente,
	codCliente,
	codPtoVend,
	sum(QuantitaSostituita) as QuantitaSostituita,
	sum(QuantitaResa) as QuantitaResa,
	sum(ConfRese) as ConfRese ,
	sum(QuantitaVenduta) as QuantitaVenduta,
	sum(QuantitaOmaggi) as QuantitaOmaggi
	from OLAPVenduto
	where
	dataDoc >='2017-01-01'
	group by cod_dtip,dataDoc,codArticolo,CodAgente,codCliente,codPtoVend,codDeposito

	union all
	select case when codDeposito = '11' then '11' else '10' end as codDeposito,
	sum(case when sum(QuantitaOmaggi) = 0.0 then 0 else 1 end) over ( Partition by codArticolo,codPtoVend order by codArticolo,codCliente,codPtoVend,dataDoc,sum(QuantitaOmaggi) desc) as value_partition,
	dataDoc,
	min(data_ord) as data_ord,
	codArticolo,
	CodAgente,
	codCliente,
	codPtoVend,
	sum(QuantitaSostituita) as QuantitaSostituita,
	sum(QuantitaResa) as QuantitaResa,
	sum(ConfRese) as ConfRese ,
	sum(QuantitaVenduta) as QuantitaVenduta,
	sum(QuantitaOmaggi) as QuantitaOmaggi
	from OLAPVenduto_Salpar
	where
	dataDoc >='2017-01-01' and codCliente not in ('C0026', 'C1000', 'C5285', 'F1000') -- and codArticolo='138716' and data_ord between '2020-01-31' and '2020-02-06'
	group by cod_dtip,dataDoc,codArticolo,CodAgente,codCliente,codPtoVend,codDeposito
	--order by codArticolo,codCliente,codPtoVend,data_ord
 )
 ,grouptmpUnificazioneDepositi as(
 select  codDeposito,value_partition,dataDoc,min(data_ord) as data_ord,codArticolo,CodAgente,codCliente,codPtoVend,
	sum(QuantitaSostituita) as QuantitaSostituita ,
	sum(QuantitaResa) as QuantitaResa,
	sum(ConfRese) as ConfRese,
	sum(QuantitaVenduta) as  QuantitaVenduta,
	sum(QuantitaOmaggi) as  QuantitaOmaggi
 from tmpUnificazioneDepositi
 group by codDeposito,value_partition,dataDoc,codArticolo,CodAgente,codCliente,codPtoVend
 )
 

,tmpSuResi as (
select  '10' as codDeposito,
dataDoc,
min(data_ord) as data_ord,
codArticolo,
CodAgente,
codCliente,
codPtoVend,
lead(sum(QuantitaSostituita),1,0) over(partition by codArticolo,codCliente,codPtoVend order by value_partition) as [QuantitaSostituita],
lead(sum(QuantitaResa),1,0) over(partition by codArticolo,codCliente,codPtoVend order by value_partition)as [QuantitaResa],
lead(sum(ConfRese),1,0) over(partition by codArticolo,codCliente,codPtoVend order by value_partition)as ConfRese,
sum(QuantitaVenduta) + isnull(sum([QuantitaOmaggi]),0.0) as QuantitaVenduta,
sum(QuantitaOmaggi) as QuantitaOmaggi
from  grouptmpUnificazioneDepositi 
group by codArticolo,CodAgente,codCliente,codPtoVend,dataDoc,value_partition
--order by codArticolo,CodAgente,codCliente,codPtoVend,value_partition
)
--articoli venduti per mese per punto vendita
/*,artVendMese as(

	select distinct 
	DATEPART(MONTH,dataDoc) as mese,DATEPART(YEAR,dataDoc) as anno,codArticolo as cod_mart,
	codDeposito as cod_mdep,CodAgente,codCliente,codPtoVend
	from tmpSuResi
	where (QuantitaVenduta>0 or QuantitaOmaggi>0) 
	)

	
	,tmpInevaso as (

	select data_ord as data_reg,cod_mart,cod_mdep,codAgente,codCliente,codPtoVend, SUM(qta_ord-qta_evasa) as inevaso,
SUM(importo_riga) as importo
from
(
select '10' as cod_mdep,
	dtb_ordt.cod_vage as codAgente,
	dtb_ordr.data_ord, dtb_ordt.cod_anag as codCliente, 
	dtb_ordt.cod_vdes as codPtoVend, 
	dtb_ordr.cod_mart, dtb_ordr.descrizione,
	dtb_ordr.unt_ord,
	dtb_ordr.qta_ord, 
	dtb_ordr.qta_evasa,
	dtb_ordr.qta_ord * (dtb_ordr.val_unt * (1-sconto5/100)* (1-sconto6/100)* (1-sconto7/100)* (1-sconto8/100)) as Importo_riga  ,
	dtb_ordr.cod_promo,
	dtb_ordr.flag_evaso
from dtb_ordt, dtb_ordr,mtb_aart
where dtb_ordt.gestione = dtb_ordr.gestione and
dtb_ordt.data_ord = dtb_ordr.data_ord and
dtb_ordt.num_ord = dtb_ordr.num_ord and
dtb_ordr.cod_mart = mtb_aart.cod_mart and
dtb_ordt.gestione = 'V' and
dtb_ordt.data_ord >= '2016/01/01'
and dtb_ordt.flag_annulla = 'N' 
and qta_ord > qta_evasa
and flag_evaso <> 'I' 

union all

select '10' as cod_mdep,
	dtb_ordt.cod_vage as codAgente,
	dtb_ordr.data_ord, dtb_ordt.cod_anag as codCliente, 
	dtb_ordt.cod_vdes as codPtoVend, 
	dtb_ordr.cod_mart, dtb_ordr.descrizione,
	dtb_ordr.unt_ord,
	dtb_ordr.qta_ord, 
	dtb_ordr.qta_evasa,
	dtb_ordr.qta_ord * (dtb_ordr.val_unt * (1-sconto5/100)* (1-sconto6/100)* (1-sconto7/100)* (1-sconto8/100)) as Importo_riga  ,
	dtb_ordr.cod_promo,
	dtb_ordr.flag_evaso
from vg_linked.salpar.dbo.dtb_ordt, vg_linked.salpar.dbo.dtb_ordr,vg_linked.salpar.dbo.mtb_aart
where dtb_ordt.gestione = dtb_ordr.gestione and
dtb_ordt.data_ord = dtb_ordr.data_ord and
dtb_ordt.num_ord = dtb_ordr.num_ord and
dtb_ordr.cod_mart = mtb_aart.cod_mart and
dtb_ordt.gestione = 'V' and
dtb_ordt.data_ord >= '2016/01/01'
and dtb_ordt.flag_annulla = 'N' 
and qta_ord > qta_evasa
and flag_evaso <> 'I' 
)q
group by data_ord,cod_mart,cod_mdep,codAgente,codCliente,codPtoVend


)
--giorni in cui il punto vendita è stato rifornito
,gg_rifornimento as( 
	insert into gg_rifornimento
	select distinct dataDoc as data_doc,
	codDeposito as cod_mdep,
	CodAgente,
	codCliente,
	codPtoVend,
	DATEPART(MONTH,dataDoc) as mese,
	DATEPART(YEAR,dataDoc) as anno

	from tmpSuResi
	where (QuantitaVenduta>0 or QuantitaOmaggi>0) 
	

	)*/
	,venditeArticoli  as (

	select distinct ga.data_doc as data_doc,
	ga.cod_mdep,
	cod_mart,
	ga.CodAgente,
	ga.codCliente,
	ga.codPtoVend
	from  artVendMese  avm inner join gg_rifornimento ga
	on ga.mese = avm.mese and ga.cod_mdep=avm.cod_mdep and ga.anno=avm.anno and ga.CodAgente =avm.CodAgente
	and ga.codCliente=avm.codCliente and ga.codPtoVend =avm.codPtoVend
	
	Union 
	select distinct data_reg,cod_mdep,cod_mart,codAgente,codCliente,codPtoVend
	from tmpInevaso  
	
	)
,venditeFinali as(
	
	select tfv.data_ord,data_doc as data_reg,
	va.cod_mdep,
	va.cod_mart,
	isnull(tfv.codAgente,va.CodAgente) as codAgente,
	va.codCliente,
	va.codPtoVend,
	isnull([QuantitaVenduta],0) as qta_venduta
   ,isnull([QuantitaSostituita],0) as [QuantitaSostituita]
   ,isnull(QuantitaResa,0) as QuantitaResa
   ,isnull(ConfRese,0)as [ConfRese]
   ,isnull([QuantitaOmaggi],0)as [QuantitaOmaggi]
	from venditeArticoli  va left join tmpSuResi tfv on va.cod_mdep=tfv.codDeposito and va.data_doc=tfv.dataDoc and va.cod_mart=tfv.codArticolo
	and  va.codCliente=tfv.codCliente and  va.codPtoVend=tfv.codPtoVend and va.codAgente=tfv.CodAgente
	where va.cod_mart is not null
	--order by cod_mart,codPtoVend,data_reg
	)
,aggiuntaPromoPrezzo as (
	
	SELECT ab.cod_mart,
	ab.cod_mdep ,
	ab.data_ord,
	ab.data_reg,
	ab.codAgente,
	ab.codCliente,
	ab.codPtoVend,
	isnull(isNull(dest.cod_vlis,clie.cod_vlis),'TD') as cod_vlis,
	DATEPART( MONTH , ab.data_reg ) as mese,
	DATEPART( DAY , ab.data_reg ) as GGmese,
	DATEPART( WEEK ,ab.data_reg ) as settimana,
	DATEPART( WEEKDAY , ab.data_reg ) as GGsettimana,
	isnull(sum_promo,0) as sum_promo,
	isnull(avg_prezzo_vendita,0.0) as avg_prezzo_vendita,
	isnull(min_prezzo_vendita,0.0) as min_prezzo_vendita,
	isnull(max_prezzo_vendita,0.0) as max_prezzo_vendita,
	isnull(avg_variazione_prezzo,0.0) as avg_variazione_prezzo,
	isnull(min_variazione_prezzo,0.0) as min_variazione_prezzo,
	isnull(max_variazione_prezzo,0.0) as max_variazione_prezzo,
	isnull(stdev_variazione_prezzo,0.0) as stdev_variazione_prezzo,
	(isnull(qta_venduta,0.0) - isnull([QuantitaSostituita],0.0) - isnull(QuantitaResa,0.0) + isnull(inevaso,0.0)) as qta_venduta
   ,isnull([QuantitaSostituita],0.0) as [QuantitaSostituita]
   ,isnull(QuantitaResa,0.0) as QuantitaResa
   ,isnull(ConfRese,0.0)as [ConfRese]
   ,isnull([QuantitaOmaggi],0.0)as [QuantitaOmaggi],
	sum(qta_venduta) OVER (Partition by ab.cod_mdep,ab.cod_mart,ab.data_reg order by ab.cod_mdep,ab.cod_mart,ab.data_reg) as qta_venduta_giornaliera,
	avg(isnull(avg_prezzo_vendita,0.0)) OVER (Partition by ab.cod_mdep,ab.cod_mart,ab.data_reg order by ab.cod_mdep,ab.cod_mart,ab.data_reg) as prezzo_giornaliero,
   inevaso,
   importo

	FROM   venditeFinali ab left outer join prezzo_vendita_articolo pva 
							on ab.cod_mart=pva.cod_mart and ab.data_reg=pva.data_reg 
						
							left join vtb_clie clie on codCliente=clie.cod_anag 
							left join vtb_dest dest on codCliente= dest.cod_anag and codPtoVend=cod_vdes
							left join tmpInevaso on ab.cod_mart=tmpInevaso.cod_mart and ab.data_reg=tmpInevaso.data_reg and ab.cod_mdep=tmpInevaso.cod_mdep 
									and ab.codCliente=tmpInevaso.codCliente and ab.codAgente=tmpInevaso.codAgente and ab.codPtoVend =tmpInevaso.codPtoVend
	WHERE ab.cod_mdep in ('10')-- and  ab.cod_mart='999900047'
	
	
	)


insert into analisi_vendite_giornaliera

SELECT ab.cod_mart,
	ab.cod_mdep ,
	isnull(ab.data_ord,ab.data_reg) as data_ord,
	ab.data_reg,
	mese,
	GGmese,
	settimana,
	GGsettimana,
	case when data_ricorrenza is null then  0 else 1 end as ricorrenza,
	isnull(ab.codAgente,'nodef') as codAgente ,
	isnull(ab.codCliente,'nodef') as codCliente,
	isnull(ab.codPtoVend,'nodef' )as codPtoVend,
	[peso],
	[descr_mgrp],
	[descr_msgr],
	[descr_msfa],
	qta_cnf,
	gg_scad_partita,
	promoNoPromo,
	sum_promo,
	isnull(avg(qta_venduta) over (partition by  DATEPART( WEEKDAY ,ab.data_reg ),ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg ROWS BETWEEN 5 PRECEDING AND 1 PRECEDING),isnull(qta_venduta,0))  as QtaMediaVendGiorn_ptoVend,
	isnull(avg(avg_prezzo_vendita) over (partition by  DATEPART( WEEKDAY ,ab.data_reg ),ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg ROWS BETWEEN 5 PRECEDING AND 1 PRECEDING),avg_prezzo_vendita) as PrezzoMedioVendGiorn,
	isnull(avg(prezzo_vendita) over (partition by  DATEPART( WEEKDAY ,ab.data_reg ),ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg ROWS BETWEEN 5 PRECEDING AND 1 PRECEDING),avg_prezzo_vendita) as PrezzoMedioVendGiorn_ptoVend,
	LAG(qta_venduta,1,0) OVER (Partition by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg,mese,GGmese,GGsettimana) as day1_ptoVend,
	LAG(qta_venduta,2,0) OVER (Partition by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg,mese,GGmese,GGsettimana) as day2_ptoVend,
	LAG(qta_venduta,3,0) OVER (Partition by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg,mese,GGmese,GGsettimana) as day3_ptoVend,
	LAG(qta_venduta,4,0) OVER (Partition by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg,mese,GGmese,GGsettimana) as day4_ptoVend,
	LAG(qta_venduta,5,0) OVER (Partition by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg,mese,GGmese,GGsettimana) as day5_ptoVend,
	LAG(qta_venduta,6,0) OVER (Partition by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg,mese,GGmese,GGsettimana) as day6_ptoVend,
	LAG(qta_venduta,7,0) OVER (Partition by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg,mese,GGmese,GGsettimana) as day7_ptoVend,
	prezzo_vendita,
	avg_prezzo_vendita,
	min_prezzo_vendita,
	max_prezzo_vendita,
	variazione_prezzo,
	avg_variazione_prezzo,
	min_variazione_prezzo,
	stdev_variazione_prezzo,
	max_variazione_prezzo,
	isnull(STDEV(qta_venduta) OVER(Partition by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg,mese,GGmese,GGsettimana ROWS BETWEEN 18 PRECEDING AND 3 PRECEDING),0) as dev_std_30,
	isnull(AVG(qta_venduta) OVER(Partition by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg,mese,GGmese,GGsettimana ROWS BETWEEN 18 PRECEDING AND 3 PRECEDING),0) as media_30,
	isnull(AVG(qta_venduta) OVER(Partition by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg,mese,GGmese,GGsettimana ROWS BETWEEN 9 PRECEDING AND 3 PRECEDING),0) as media_15,
	isnull(AVG(qta_venduta) OVER(Partition by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend order by ab.cod_mdep,ab.cod_mart,ab.codAgente,ab.codCliente,ab.codPtoVend,ab.data_reg,mese,GGmese,GGsettimana ROWS BETWEEN 6 PRECEDING AND 3 PRECEDING),0) as media_7
   ,[QuantitaSostituita]
   ,QuantitaResa
   ,[ConfRese]
   ,[QuantitaOmaggi]
   ,isnull(inevaso,0) as inevaso,
    isnull(importo,0) as importo_inevaso
	,qta_venduta,
	null as lag_cosnegna_medio

  FROM   aggiuntaPromoPrezzo ab 
					
						left join vgalimenti_NN_project.dbo.ricorrenze on ab.data_reg=data_ricorrenza 
						left join vgalimenti_NN_project.dbo.articoli_movimentati art on ab.cod_mart=art.cod_mart 
						left join prezzo_vendita_articolo_listino pval
							on ab.cod_mart=pval.cod_mart and ab.data_reg=pval.data_reg and ab.cod_vlis=pval.cod_vlis
  WHERE ab.cod_mdep in ('10') 
  and ab.data_reg >='2017-01-01'  
  
  order by ab.cod_mdep,ab.cod_mart,ab.data_reg 

