SET DATEFIRST  1; 
 SELECT cod_mart, 
 	cod_mdep , 
 	DATEPART(YEAR,data_reg) as anno, 
 	DATEPART(week,data_reg) as settimana, 
 	sum(ricorrenza) as ricorrenza, 
 	codAgente, 
 	codCliente, 
 	codPtoVend, 
 	[peso], 
 	[descr_mgrp], 
 	[descr_msgr], 
 	[descr_msfa], 
 	min(qta_cnf) as qta_cnf, 
 	min(gg_scad_partita) as gg_scad_partita , 
 	sum(promoNoPromo) as promoNoPromo, 
 	sum(sum_promo) as sum_promo, 
 	avg(QtaMediaVendGiorn_ptoVend) as QtaMediaVendGiorn_ptoVend , 
 	avg (PrezzoMedioVendGiorn) as PrezzoMedioVendGiorn, 
 	avg(PrezzoMedioVendGiorn_ptoVend) as PrezzoMedioVendGiorn_ptoVend, 
 	LAG(sum(qta_venduta),1,0) OVER (Partition by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend order by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend,DATEPART(YEAR,data_reg),DATEPART(week,data_reg)) as week1_ptoVend, 
 	LAG(sum(qta_venduta),2,0) OVER (Partition by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend order by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend,DATEPART(YEAR,data_reg),DATEPART(week,data_reg)) as week2_ptoVend, 
 	LAG(sum(qta_venduta),3,0) OVER (Partition by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend order by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend,DATEPART(YEAR,data_reg),DATEPART(week,data_reg)) as week3_ptoVend, 
 	LAG(sum(qta_venduta),4,0) OVER (Partition by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend order by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend,DATEPART(YEAR,data_reg),DATEPART(week,data_reg)) as week4_ptoVend, 
 	LAG(sum(qta_venduta),5,0) OVER (Partition by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend order by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend,DATEPART(YEAR,data_reg),DATEPART(week,data_reg)) as week5_ptoVend, 
 	LAG(sum(qta_venduta),6,0) OVER (Partition by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend order by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend,DATEPART(YEAR,data_reg),DATEPART(week,data_reg)) as week6_ptoVend, 
 	LAG(sum(qta_venduta),7,0) OVER (Partition by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend order by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend,DATEPART(YEAR,data_reg),DATEPART(week,data_reg)) as week7_ptoVend, 
 	avg(prezzo_vendita) as prezzo_vendita, 
 	avg(avg_prezzo_vendita) as avg_prezzo_vendita, 
 	avg(min_prezzo_vendita) as min_prezzo_vendita, 
 	avg(max_prezzo_vendita) as max_prezzo_vendita, 
 	avg(variazione_prezzo) as variazione_prezzo, 
 	avg(avg_variazione_prezzo) as avg_variazione_prezzo, 
 	avg(min_variazione_prezzo) as min_variazione_prezzo, 
 	avg(max_variazione_prezzo) as max_variazione_prezzo, 
 	avg(stdev_variazione_prezzo) as stdev_variazione_prezzo, 
 	LAG(sum(dev_std_30),1,0) OVER (Partition by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend order by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend,DATEPART(YEAR,data_reg),DATEPART(week,data_reg)) as dev_std_30, 
 	LAG(sum(media_30),1,0) OVER (Partition by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend order by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend,DATEPART(YEAR,data_reg),DATEPART(week,data_reg)) as media_30, 
 	LAG(sum(media_15),1,0) OVER (Partition by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend order by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend,DATEPART(YEAR,data_reg),DATEPART(week,data_reg)) as media_15, 
 	LAG(sum(media_7),1,0) OVER (Partition by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend order by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend,DATEPART(YEAR,data_reg),DATEPART(week,data_reg)) as media_7, 
 	sum([QuantitaSostituita]) as [QuantitaSostituita] 
 	,sum(inevaso) as inevaso 
    ,sum(QuantitaResa) as QuantitaResa 
    ,sum([ConfRese]) as [ConfRese] 
    ,sum([QuantitaOmaggi]) as [QuantitaOmaggi] 
 	,sum(qta_venduta)   as qta_venduta 
   FROM    [dbo].[analisi_vendite_giornaliera] 
   group by cod_mdep,cod_mart,DATEPART(YEAR,data_reg),DATEPART(week,data_reg),[peso], 
   descr_mgrp,descr_msgr,descr_msfa,codAgente,codCliente,codPtoVend
    order by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend,anno,DATEPART(week,data_reg)

