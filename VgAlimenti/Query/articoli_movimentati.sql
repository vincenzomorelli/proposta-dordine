--lasciare commentato il flag_stato perch� abbiamo necessit� di considerare tutti gli articoli per il momento
CREATE TABLE articoli_movimentati(
	cod_mart varchar(15) not null,
	descrizione varchar(200),
	descrizione_estesa varchar(300),
	unt_mis varchar(15), 
	unt_mis2 varchar(15), 
	rap_conv2 float,
	peso float, 
	cod_mgrp varchar(15),
	descr_mgrp varchar(200),
	cod_msgr varchar(15),
	descr_msgr varchar (200),
	cod_msfa varchar(15),
	descr_msfa varchar(200),
	gg_scad_partita int,
	peso_kg float,
	qta_cnf float

)

insert into articoli_movimentati 
select cod_mart, 
mtb_aart.descrizione, 
descrizione_estesa, 
unt_mis, 
unt_mis2, 
rap_conv2,
case when mtb_aart.rap_conv2 <> 1 and mtb_aart.rap_conv2 <> 0 then round(1/mtb_aart.rap_conv2, 3) else 1 end as peso, 
mtb_aart.cod_mgrp,
mtb_grup.descrizione as descr_mgrp,
mtb_aart.cod_msgr,
mtb_sgrp.descrizione as descr_msgr,
mtb_aart.cod_msfa,
mtb_aart.descrizione as descr_msfa,
mtb_aart.gg_scad_partita,
peso_kg,
qta_cnf
from  mtb_aart  inner join mtb_grup on mtb_aart.cod_mgrp = mtb_grup.cod_mgrp
				inner join mtb_sgrp  on mtb_aart.cod_mgrp = mtb_sgrp.cod_mgrp and
										mtb_aart.cod_msgr = mtb_sgrp.cod_msgr
				left outer join  mtb_sfam on mtb_aart.cod_mgrp = mtb_sfam.cod_mgrp and
											mtb_aart.cod_msgr = mtb_sfam.cod_msgr and
											mtb_aart.cod_msfa = mtb_sfam.cod_msfa
--where flag_stato = 'A' 
and cod_mart in (select cod_mart from mtb_lisv_data )
and cod_mart in (SELECT cod_mart FROM mtb_sart wHERE qta_scar > 0 and anno >= 2017 --and cod_mdep = '10' 
						union all
						SELECT mtb_comp.cod_mart 
						FROM mtb_sart left outer join  mtb_comp on mtb_sart.cod_mart = mtb_comp.cod_comp 
						wHERE qta_scar > 0 and anno >= 2017-- and cod_mdep = '10' 
						)



vtb_list,vtb_list_data,mtb_lisv_data,vtb_promo,mtb_movi,mtb_sart