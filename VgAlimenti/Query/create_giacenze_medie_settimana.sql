--create table giacenze_medie
with temp_giacenze_salpar ([cod_mart],[cod_mdep],[data_reg],[qta_acq],[qta_venduta]) as (
	Select cod_mart,
	cod_mdep,
	'2016/01/01' as data_reg,
	qta_iniz+qta_car-qta_Scar as qta_acq,
	0.00 as qta_venduta
	from salpar.dbo.mtb_sart
	Where
		anno = year('2015/01/01') and
		qta_iniz+qta_car-qta_Scar > 0 and
		(qta_iniz+qta_car_costo) > 0 --and cod_mart = @cod_mart
		and cod_mdep in  ('10')
	UNION ALL
	Select cod_mart,cod_mdep,data_reg,qta_car,qta_scar
	from salpar.dbo.mtb_movi
	where data_reg>='2016-01-01' --and cod_mart= @cod_mart
	and cod_mdep in  ('10')
	)
,temp_salpar_date as (
select distinct cod_mart ,v.d
from temp_giacenze_salpar cross apply ( select d from vgalimenti.dbo.getTableDate('2016-01-01',GETDATE())) v
)
, tempgiacenze_salpar as (
	select  tvg.cod_mart as cod_mart,
	'10' as cod_mdep,
	Dateadd(day,1,tvg.d) as data_reg,
	sum(sum(isnull(qta_acq,0)-isnull(qta_venduta,0))) OVER (partition by  tvg.cod_mart order by  tvg.cod_mart, tvg.d ) as giacenza_al_mattino
	--qta_acq,qta_venduta
	From  temp_salpar_date  tvg left outer join  temp_giacenze_salpar tg on d=data_reg and tvg.cod_mart=tg.cod_mart
    and  data_reg<='2020-11-01'
	group by  tvg.cod_mart,data_reg,d
	--order by tvg.cod_mart,tvg.d
)
,  temp_giacenze_VG ([cod_mart],[cod_mdep],[data_reg],[qta_acq],[qta_venduta]) as (
	Select cod_mart,
	cod_mdep,
	'2016/01/01' as data_reg,
	qta_iniz+qta_car-qta_Scar as qta_acq,
	0.00 as qta_venduta
	from mtb_sart
	Where
		anno = year('2015/01/01') and
		qta_iniz+qta_car-qta_Scar > 0 and
		(qta_iniz+qta_car_costo) > 0 --and cod_mart = @cod_mart
		and cod_mdep in  ('10')
	UNION ALL
	Select cod_mart,cod_mdep,data_reg,qta_car,qta_scar
	from mtb_movi
	where data_reg>='2016-01-01' --and cod_mart= @cod_mart
	and cod_mdep in  ('10')
	)

, temp_vg_date as (
select distinct cod_mart ,v.d
from temp_giacenze_VG cross apply ( select d from vgalimenti.dbo.getTableDate('2016-01-01',GETDATE())) v
)

, tempgiacenze_VG as (
	select  tvg.cod_mart as cod_mart,
	'10' as cod_mdep,
	Dateadd(day,1,tvg.d) as data_reg,
	sum(sum(isnull(qta_acq,0)-isnull(qta_venduta,0))) OVER (partition by  tvg.cod_mart order by  tvg.cod_mart, tvg.d ) as giacenza_al_mattino
	--qta_acq,qta_venduta
	From  temp_vg_date  tvg left outer join  temp_giacenze_VG tg on d=data_reg and tvg.cod_mart=tg.cod_mart
    and  data_reg<='2020-11-01'
	group by  tvg.cod_mart,data_reg,d
	--order by tvg.cod_mart,tvg.d
)

, ordr_codici as (
select 
		distinct cod_mart, data_ord
from dtb_ordr
where data_ord >= '2020-01-01' and gestione = 'A' and flag_evaso = 'E' 

union all

select 
		distinct cod_mart,
		data_ord
from salpar.dbo.dtb_ordr
where data_ord >= '2020-01-01' and gestione = 'A' and flag_evaso = 'E' 

--order by cod_mart

)
, giacenze_combinate as (
select 
		isnull(v.cod_mart, s.cod_mart) as cod_mart,
		isnull(v.data_reg, s.data_reg) as data_reg,
		isnull(s.giacenza_al_mattino,0)+isnull(v.giacenza_al_mattino,0) as giacenza_al_mattino
from tempgiacenze_VG v
full outer join tempgiacenze_salpar s on v.cod_mart = s.cod_mart and v.data_reg = s.data_reg
where (v.data_reg >= '2020-01-01' or s.data_reg >= '2020-01-01') 

)

, giacenze_medie as (
select
		g.cod_mart,
		avg(g.giacenza_al_mattino) as giacenza_media,
		count(*) as conto_ordini --into giacenze_medie_2020
from giacenze_combinate g
inner join ordr_codici o on o.cod_mart = g.cod_mart and g.data_reg = o.data_ord
group by g.cod_mart 
)


, predizioni_sett as (
select
		cod_mart, 
		data_predizione,
		settimana,
		predizione as predizione_settimana
from  Predizione_settimanale 
where data_predizione between '2020-10-01' and '2020-11-01'
)

, quantita_vendute as (
select 
		cod_mart, 
		--data_reg, 
		settimana,
		sum(qta_venduta) as qta_venduta
from analisi_vendite_giornaliera 
where data_ord > '2020-09-01' 
and settimana in (select distinct settimana from predizioni_sett) 
group by cod_mart, settimana
)

, confronto as (
select 
		p.cod_mart,
		p.data_predizione,
		p.settimana,
		predizione_settimana,
		q.qta_venduta,
		predizione_settimana - q.qta_venduta as errore_settimana
from predizioni_sett p
left join quantita_vendute q on p.cod_mart = q.cod_mart and p.settimana = q.settimana
--order by data_predizione, settimana
)

, errore_max as (
select
		cod_mart,
		--settimana,
		abs(min(predizione_settimana - qta_venduta)) as errore_max_settimana
from confronto
group by cod_mart
)

select
		e.cod_mart,
		errore_max_settimana,
		giacenza_media,
		case when giacenza_media > 0 and errore_max_settimana > 0 and errore_max_settimana / giacenza_media < 0.84 then round(errore_max_settimana / giacenza_media, 4) 
			else 0.84 end as rapporto_errore_giacenza_settimana
from errore_max e
left join giacenze_medie g on g.cod_mart = e.cod_mart
order by rapporto_errore_giacenza_settimana desc