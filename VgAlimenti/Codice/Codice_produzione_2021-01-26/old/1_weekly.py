# -*- coding: utf-8 -*-
"""
Created on Thu Jul  2 17:01:06 2020

@author: Administrator
"""

## -*- coding: utf-8 -*-
#"""
##Created on Mon Oct 29 09:03:33 2018
##
##@author: Vincenzo Morelli
##"""
from import_modules import *
from email_sender import *


def RMSLE(y_true, y_pred):
        return K.sqrt(K.mean(K.square(K.log(1+y_true) - K.log(1+y_pred)))) 

server = '192.168.102.214\SQL2014'
database ='vgalimenti_NN_project'
username = 'sa'
password = 'sa'
connectionString = 'DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+password


apikey = '679f07cc6e387621a41b11bf17989d90'
citta='BARI'
dep_cit = np.load('C:/Users/Administrator/Desktop/Produzione/VGAlimenti/Utils/dep_cit.npy',allow_pickle=True).item()


date_current_m1= datetime.now()
date_current_str_m1=date_current_m1.strftime('%Y-%m-%d')

cnxn = pyodbc.connect(connectionString)
start=str(datetime.now().weekday()+1)
df=pd.read_sql_query("SET DATEFIRST " +start+"; "
+ "SELECT cod_mart, "
+ "	cod_mdep , "
+ "	DATEPART(YEAR,lag_consegna_medio) as anno, "
+ "	datepart(week,lag_consegna_medio) as settimana, "
+ "	sum(ricorrenza) as ricorrenza, "
+ "	codAgente, "
+ "	codCliente, "
+ "	codPtoVend, "
+ "	[peso], "
+ "	[descr_mgrp], "
+ "	[descr_msgr], "
+ "	[descr_msfa], "
+ "	min(qta_cnf) as qta_cnf, "
+ "	min(gg_scad_partita) as gg_scad_partita , "
+ "	sum(promoNoPromo) as promoNoPromo, "
+ "	sum(sum_promo) as sum_promo, "
+ "	avg(QtaMediaVendGiorn_ptoVend) as QtaMediaVendGiorn_ptoVend , "
+ "	avg (PrezzoMedioVendGiorn) as PrezzoMedioVendGiorn, "
+ "	avg(PrezzoMedioVendGiorn_ptoVend) as PrezzoMedioVendGiorn_ptoVend, "
+ "	LAG(sum(qta_venduta),1,0) OVER (Partition by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend order by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend,DATEPART(YEAR,lag_consegna_medio),datepart(week,lag_consegna_medio)) as week1_ptoVend, "
+ "	LAG(sum(qta_venduta),2,0) OVER (Partition by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend order by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend,DATEPART(YEAR,lag_consegna_medio),datepart(week,lag_consegna_medio)) as week2_ptoVend, "
+ "	LAG(sum(qta_venduta),3,0) OVER (Partition by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend order by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend,DATEPART(YEAR,lag_consegna_medio),datepart(week,lag_consegna_medio)) as week3_ptoVend, "
+ "	LAG(sum(qta_venduta),4,0) OVER (Partition by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend order by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend,DATEPART(YEAR,lag_consegna_medio),datepart(week,lag_consegna_medio)) as week4_ptoVend, "
+ "	LAG(sum(qta_venduta),5,0) OVER (Partition by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend order by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend,DATEPART(YEAR,lag_consegna_medio),datepart(week,lag_consegna_medio)) as week5_ptoVend, "
+ "	LAG(sum(qta_venduta),6,0) OVER (Partition by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend order by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend,DATEPART(YEAR,lag_consegna_medio),datepart(week,lag_consegna_medio)) as week6_ptoVend, "
+ "	LAG(sum(qta_venduta),7,0) OVER (Partition by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend order by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend,DATEPART(YEAR,lag_consegna_medio),datepart(week,lag_consegna_medio)) as week7_ptoVend, "
+ "	avg(prezzo_vendita) as prezzo_vendita, "
+ "	avg(avg_prezzo_vendita) as avg_prezzo_vendita, "
+ "	avg(min_prezzo_vendita) as min_prezzo_vendita, "
+ "	avg(max_prezzo_vendita) as max_prezzo_vendita, "
+ "	avg(variazione_prezzo) as variazione_prezzo, "
+ "	avg(avg_variazione_prezzo) as avg_variazione_prezzo, "
+ "	avg(min_variazione_prezzo) as min_variazione_prezzo, "
+ "	avg(max_variazione_prezzo) as max_variazione_prezzo, "
+ "	avg(stdev_variazione_prezzo) as stdev_variazione_prezzo, "
+ "	LAG(sum(dev_std_30),1,0) OVER (Partition by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend order by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend,DATEPART(YEAR,lag_consegna_medio),datepart(week,lag_consegna_medio)) as dev_std_30, "
+ "	LAG(sum(media_30),1,0) OVER (Partition by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend order by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend,DATEPART(YEAR,lag_consegna_medio),datepart(week,lag_consegna_medio)) as media_30, "
+ "	LAG(sum(media_15),1,0) OVER (Partition by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend order by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend,DATEPART(YEAR,lag_consegna_medio),datepart(week,lag_consegna_medio)) as media_15, "
+ "	LAG(sum(media_7),1,0) OVER (Partition by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend order by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend,DATEPART(YEAR,lag_consegna_medio),datepart(week,lag_consegna_medio)) as media_7, "
+ "	sum([QuantitaSostituita]) as [QuantitaSostituita] "
+ "	,sum(inevaso) as inevaso "
+ "   ,sum(QuantitaResa) as QuantitaResa "
+ "   ,sum([ConfRese]) as [ConfRese] "
+ "   ,sum([QuantitaOmaggi]) as [QuantitaOmaggi] "
+ "	,sum(qta_venduta)   as qta_venduta "
+ "  FROM    [dbo].[analisi_vendite_giornaliera] "
+ " Where (lag_consegna_medio>='2020-01-01' and lag_consegna_medio<dateadd(day,7,'"+date_current_str_m1+"')) "
+ " and not (data_ord<'"+date_current_str_m1+"' and data_reg>='"+date_current_str_m1+"') "
+ "  group by cod_mdep,cod_mart,DATEPART(YEAR,lag_consegna_medio),datepart(week,lag_consegna_medio),[peso], "
+ "  descr_mgrp,descr_msgr,descr_msfa,codAgente,codCliente,codPtoVend"
+ "    order by cod_mdep,cod_mart,codAgente,codCliente,codPtoVend,anno,settimana",cnxn)

df=df[df.qta_venduta>=0]

#DATEADD(month,-3,GETDATE())
def creaVariabile_qtaVend_SettPrec(df):
    grouped_df = df.groupby(['cod_mart','cod_mdep','anno','settimana'],as_index=False)['qta_venduta'].sum()
    grouped_df['qta_venduta_SettPrec'] = grouped_df\
                                .sort_values(['cod_mart','cod_mdep','anno','settimana'])\
                                .qta_venduta.shift(1)
    df1 = pd.merge(df, grouped_df[['cod_mart','cod_mdep','anno','settimana','qta_venduta_SettPrec']],
               how='left',
               on=['cod_mart','cod_mdep','anno','settimana'])
    return df1

def creaVariabile_qtaVend_SettPrec_Agente(data_frame):
    grouped_df2 = data_frame.groupby(['codAgente','anno','settimana'],as_index=False)['qta_venduta'].sum()    
    grouped_df2['qta_vendutaAgente_SettPrec'] = grouped_df2\
                                .sort_values(['codAgente','anno','settimana'])\
                                .qta_venduta.shift(1)
    df1 = pd.merge(data_frame, grouped_df2[['codAgente','anno','settimana','qta_vendutaAgente_SettPrec']],
               how='left',
               on=['codAgente','anno','settimana'])
    
    
    return df1
def creaVariabile_qtaVend_SettPrec_PtoVend(data_frame):
    grouped_df2 = data_frame.groupby(['codPtoVend','anno','settimana'],as_index=False)['qta_venduta'].sum()    
    grouped_df2['qta_vendutaPtoVend_SettPrec'] = grouped_df2\
                                .sort_values(['codPtoVend','anno','settimana'])\
                                .qta_venduta.shift(1)
    df1 = pd.merge(data_frame, grouped_df2[['codPtoVend','anno','settimana','qta_vendutaPtoVend_SettPrec']],
               how='left',
               on=['codPtoVend','anno','settimana'])
    
    
    return df1

def creaVariabile_qtaVend_SettPrec_Cliente(data_frame):
    grouped_df2 = data_frame.groupby(['codCliente','anno','settimana'],as_index=False)['qta_venduta'].sum()    
    grouped_df2['qta_vendutaCliente_SettPrec'] = grouped_df2\
                                .sort_values(['codCliente','anno','settimana'])\
                                .qta_venduta.shift(1)
    df1 = pd.merge(data_frame, grouped_df2[['codCliente','anno','settimana','qta_vendutaCliente_SettPrec']],
               how='left',
               on=['codCliente','anno','settimana'])
    
    
    return df1

clusters=pd.read_csv('C:/Users/Administrator/Desktop/Produzione/VGAlimenti/Clusters/clusters_1_weekly',dtype={'cod_mart':str,'cod_mdep':str,'codAgente':str,'codPtoVend':str})    
#df['cod_mart']=df.cod_mart.where(df.cod_mart.str[:1]!=str(0),df.cod_mart.str[1:])
df = pd.merge(df,clusters,on=['cod_mdep','cod_mart','codAgente', 'descr_mgrp', 'descr_msgr', 'descr_msfa','codCliente','codPtoVend'],how='left')
df['cod_mart']=df['cod_mart'].astype(str)
df=df[~df.cluster.isna()]
df['cod_mdep']=df['cod_mdep'].astype('int64')

df=creaVariabile_qtaVend_SettPrec(df)
df=creaVariabile_qtaVend_SettPrec_Agente(df)
df=creaVariabile_qtaVend_SettPrec_PtoVend(df)
df=creaVariabile_qtaVend_SettPrec_Cliente(df)
df['qta_vendutaCliente_SettPrec'].fillna(0,inplace=True)
df['qta_vendutaAgente_SettPrec'].fillna(0,inplace=True)
df['qta_vendutaPtoVend_SettPrec'].fillna(0,inplace=True)
df['qta_venduta_SettPrec'].fillna(0,inplace=True)


df=df[df.anno==datetime.now().year]
df=df[df.settimana==df.settimana.max()]




#sales_mapping_dep = np.load('C:/Users/Administrator/Desktop/Carelli_Proposta_ordine/Algoritmi/dictionaries/sales_mapping_dep.npy').item()
#df['cod_mdep'] = df['cod_mdep'].map(sales_mapping_dep).astype(int)

unique_dict = np.load('C:/Users/Administrator/Desktop/Produzione/VGAlimenti/Dizionari/1_weekly/unique_dict.npy',allow_pickle=True).item()

df['descr_mgrp'].fillna(0,inplace=True)
df['descr_msgr'].fillna(0,inplace=True)
df['descr_msfa'] .fillna(0,inplace=True)


agente = np.load('C:/Users/Administrator/Desktop/Produzione/VGAlimenti/Dizionari/1_weekly/agent.npy',allow_pickle=True).item()
cliente = np.load('C:/Users/Administrator/Desktop/Produzione/VGAlimenti/Dizionari/1_weekly/cliente.npy',allow_pickle=True).item()
ptoVend = np.load('C:/Users/Administrator/Desktop/Produzione/VGAlimenti/Dizionari/1_weekly/ptoVend.npy',allow_pickle=True).item()
df['codAgente'] = df['codAgente'].map(agente).astype(int)
df['codCliente'] = df['codCliente'].map(cliente).astype(int)
df['codPtoVend'] = df['codPtoVend'].map(ptoVend).astype(int)

df['avg_prezzo_vendita'].fillna(0.0,inplace=True)
df['peso'].fillna(1.0,inplace=True)
df['promoNoPromo'].fillna(0.0,inplace=True)
df['variazione_prezzo'].fillna(0.0,inplace=True)
df['prezzo_vendita'].fillna(0.0,inplace=True)


df_result=df[['cod_mart','cod_mdep','anno','settimana','descr_mgrp','descr_msfa']]
startTime = datetime.now()
X_test=df[['cod_mdep', 'settimana', 'ricorrenza', 'codAgente', 'codCliente',
       'codPtoVend', 'peso', 'qta_cnf', 'gg_scad_partita', 'promoNoPromo',
       'sum_promo', 'QtaMediaVendGiorn_ptoVend', 'PrezzoMedioVendGiorn',
       'week1_ptoVend', 'week2_ptoVend', 'week3_ptoVend', 'week4_ptoVend',
       'week5_ptoVend', 'week6_ptoVend', 'week7_ptoVend', 'prezzo_vendita',
       'avg_prezzo_vendita', 'min_prezzo_vendita', 'max_prezzo_vendita',
       'variazione_prezzo', 'avg_variazione_prezzo', 'min_variazione_prezzo',
       'max_variazione_prezzo', 'stdev_variazione_prezzo', 'dev_std_30',
       'media_30', 'media_15', 'media_7', 'cluster', 'qta_venduta_SettPrec',
       'qta_vendutaAgente_SettPrec', 'qta_vendutaPtoVend_SettPrec',
       'qta_vendutaCliente_SettPrec', 'descr_mgrp', 'descr_msgr',
       'descr_msfa']]

X_test['cod_mdep']=0
X_test['descr_mgrp'] = X_test['descr_mgrp'].map(unique_dict).astype(int)
X_test['descr_msgr'] = X_test['descr_msgr'].map(unique_dict).astype(int)
X_test['descr_msfa'] = X_test['descr_msfa'].map(unique_dict).astype(int)

xg_reg= xgb.Booster({'nthread': 4})
model='C:/Users/Administrator/Desktop/Produzione/VGAlimenti/Modelli/1_weekly/'+str(start)+'_model.json'
xg_reg.load_model(model)


data_dmatrix = xgb.DMatrix(data=X_test)
y_val= xg_reg.predict(data_dmatrix)
    
df_result['predizione']=y_val
df_result['data_predizione']=date_current_str_m1

df_result=df_result.groupby(['cod_mdep','anno','settimana','cod_mart','descr_mgrp','descr_msfa','data_predizione']).sum().reset_index()      
#df_result.to_excel("output.xlsx")
df_result['predizione']=np.around(df_result.predizione)

df_mail=df_result.drop(['anno','data_predizione','settimana'], axis=1).sort_values(by=['descr_mgrp','descr_msfa','cod_mart','cod_mdep'])

df_result.drop(['descr_mgrp','descr_msfa'], axis=1, inplace=True) 
df_result['giorno']=start    
endTime = datetime.now()
engine = sqlalchemy.create_engine("mssql+pyodbc://" + 'sa' + ":" + 'sa' + "@" + server + "/" + 'vgalimenti_NN_project' + "?driver=SQL+Server")
df_result.to_sql("Predizione_settimanale",engine,if_exists='append',index=False)
print('Execution: ' + str(endTime - startTime))

ref=pd.read_excel('C:/Users/Administrator/Desktop/Produzione/VGAlimenti/Utils/referenze_da_esaminare.xlsx')
df_mail=df_mail[df_mail.cod_mart.isin(ref.cod_mart)]
writer = pd.ExcelWriter("output.xlsx", engine='openpyxl')
df_mail.to_excel(writer, sheet_name='Predizioni_settimanali',index=False)  # send df to writer
writer.sheets['Predizioni_settimanali'].column_dimensions['A'].width = 15
writer.sheets['Predizioni_settimanali'].column_dimensions['B'].width = 15
writer.sheets['Predizioni_settimanali'].column_dimensions['C'].width = 50
writer.sheets['Predizioni_settimanali'].column_dimensions['D'].width = 50
writer.sheets['Predizioni_settimanali'].column_dimensions['E'].width = 20

writer.save()
#send_email('vincenzomorelli@live.com','Predizioni_settimanali_'+date_current_str_m1,('Buongiorno Antonio, \n\nin allegato puoi trovare le previsioni settimanali aggiornate ad oggi:  ' + date_current_str_m1 +'.\n\nBuona Giornata,\n\nVincenzo'),'C:/Users/Administrator/Desktop/VG_Project/Agloritmi/output.xlsx')    


        
           
		
      
        

                       