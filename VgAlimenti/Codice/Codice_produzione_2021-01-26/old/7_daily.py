# -*- coding: utf-8 -*-
"""
Created on Thu Jul  2 17:01:06 2020

@author: Administrator
"""

## -*- coding: utf-8 -*-
#"""
##Created on Mon Oct 29 09:03:33 2018
##
##@author: Vincenzo Morelli
##"""
from import_modules import *
from email_sender import *



def RMSLE(y_true, y_pred):
        return K.sqrt(K.mean(K.square(K.log(1+y_true) - K.log(1+y_pred)))) 

def creaVariabile_qtaVend_GGprec(df):
    grouped_df = df.groupby(['cod_mart','cod_mdep','anno','mese','ggmese'],as_index=False)['qta_venduta'].sum()
    grouped_df['qta_venduta_GGprec'] = grouped_df\
                                .sort_values(['cod_mart','cod_mdep','anno','mese','ggmese'])\
                                .qta_venduta.shift(1)
    df1 = pd.merge(df, grouped_df[['cod_mart','cod_mdep','anno','mese','ggmese','qta_venduta_GGprec']],
               how='left',
               on=['cod_mart','cod_mdep','anno','mese','ggmese'])
    return df1

def creaVariabile_qtaVend_GGprec_Agente(data_frame):
    grouped_df2 = data_frame.groupby(['codAgente','anno','mese','ggmese'],as_index=False)['qta_venduta'].sum()    
    grouped_df2['qta_vendutaAgente_GGprec'] = grouped_df2\
                                .sort_values(['codAgente','anno','mese','ggmese'])\
                                .qta_venduta.shift(1)
    df1 = pd.merge(data_frame, grouped_df2[['codAgente','anno','mese','ggmese','qta_vendutaAgente_GGprec']],
               how='left',
               on=['codAgente','anno','mese','ggmese'])
    
    
    return df1

def creaVariabile_qtaVend_GGprec_PtoVend(data_frame):
    grouped_df2 = data_frame.groupby(['codPtoVend','anno','mese','ggmese'],as_index=False)['qta_venduta'].sum()    
    grouped_df2['qta_vendutaPtoVend_GGprec'] = grouped_df2\
                                .sort_values(['codPtoVend','anno','mese','ggmese'])\
                                .qta_venduta.shift(1) 
    df1 = pd.merge(data_frame, grouped_df2[['codPtoVend','anno','mese','ggmese','qta_vendutaPtoVend_GGprec']],
               how='left',
               on=['codPtoVend','anno','mese','ggmese'])
    
    
    return df1

def creaVariabile_qtaVend_GGprec_codCliente(df):
    grouped_df2 = df.groupby(['codCliente','anno','mese','ggmese'],as_index=False)['qta_venduta'].sum()    
    grouped_df2['qta_vendutaCliente_GGprec'] = grouped_df2\
                                .sort_values(['codCliente','anno','mese','ggmese'])\
                                .qta_venduta.shift(1)
    df1 = pd.merge(df, grouped_df2[['codCliente','anno','mese','ggmese','qta_vendutaCliente_GGprec']],
               how='left',
               on=['codCliente','anno','mese','ggmese'])
    
    
    return df1

def creaVariabile_qtaVend_SETTprec(df):
    grouped_df2 = df.groupby(['cod_mart','cod_mdep','settimana'],as_index=False)['qta_venduta'].sum()    
    grouped_df2['qta_venduta_settPrec'] = grouped_df2\
                                .sort_values(['cod_mart','cod_mdep','settimana'])\
                                .qta_venduta.shift(1)
    df1 = pd.merge(df, grouped_df2[['cod_mart','cod_mdep','settimana','qta_venduta_settPrec']],
               how='left',
               on=['cod_mart','cod_mdep','settimana'])
    
    
    return df1

server = '192.168.102.214\SQL2014'
database ='vgalimenti_NN_project'
username = 'sa'
password = 'sa'
connectionString = 'DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+password


apikey = '679f07cc6e387621a41b11bf17989d90'
citta='BARI'
dep_cit = np.load('C:/Users/Administrator/Desktop/Produzione/VGAlimenti/Utils/dep_cit.npy',allow_pickle=True).item()


date_current_m1= datetime.now()
date_current_str_m1=date_current_m1.strftime('%Y-%m-%d')

cnxn = pyodbc.connect(connectionString)

df=pd.read_sql_query("declare @data_today DATETIME;"
+" set @data_today='"+date_current_str_m1+"';"
+" select * from [dbo].[analisi_vendite_giornaliera] "
+" where data_reg>=DATEADD(MONTH, -1, @data_today) "
+" and not ((data_ord< @data_today and data_reg>= @data_today) or ( data_reg>dateadd(day,6,@data_today)))",cnxn)

#df=df[(df.data_reg<date_current_str_m1) | ((df.data_reg>=date_current_str_m1) & (df.qta_venduta<=0))]

tmp=df.loc[df.data_reg>=date_current_str_m1]
tmp['data_reg']=tmp.lag_consegna_medio
tmp['mese']=tmp.lag_consegna_medio.dt.month
tmp['ggmese']=tmp.lag_consegna_medio.dt.day
tmp['settimana']=tmp.lag_consegna_medio.dt.week
tmp['GGsettimana']=tmp.lag_consegna_medio.dt.dayofweek
tmp.dropna(inplace=True) 
df=df.loc[df.data_reg<date_current_str_m1]
df=pd.concat([df,tmp], ignore_index=True)

df = df[['cod_mart', 'cod_mdep', 'data_reg', 'mese', 'ggmese', 'settimana',
       'GGsettimana', 'ricorrenza', 'codAgente', 'codCliente', 'codPtoVend',
       'peso', 'descr_mgrp', 'descr_msgr', 'descr_msfa', 'qta_cnf',
       'gg_scad_partita', 'promoNoPromo', 'sum_promo','QtaMediaVendGiorn_ptoVend',
       'PrezzoMedioVendGiorn', 'day1_ptoVend', 'day2_ptoVend', 'day3_ptoVend',
       'day4_ptoVend', 'day5_ptoVend', 'day6_ptoVend', 'day7_ptoVend',
       'prezzo_vendita', 'avg_prezzo_vendita', 'min_prezzo_vendita',
       'max_prezzo_vendita', 'variazione_prezzo', 'avg_variazione_prezzo',
       'min_variazione_prezzo', 'max_variazione_prezzo',
       'stdev_variazione_prezzo', 'dev_std_30', 'media_30', 'media_15',
       'media_7','qta_venduta']]

df['anno']=pd.DatetimeIndex(df['data_reg']).year
df['anno'] = df['anno'].astype('int')
df=creaVariabile_qtaVend_GGprec(df)
df=creaVariabile_qtaVend_GGprec_Agente(df)
df=creaVariabile_qtaVend_GGprec_PtoVend(df)
df=creaVariabile_qtaVend_GGprec_codCliente(df)
df=creaVariabile_qtaVend_SETTprec(df)
df['qta_vendutaAgente_GGprec'].fillna(0,inplace=True)
df['qta_vendutaPtoVend_GGprec'].fillna(0,inplace=True)
df['qta_venduta_GGprec'].fillna(0,inplace=True)
df['qta_venduta_settPrec'].fillna(0,inplace=True)
df['qta_vendutaCliente_GGprec'].fillna(0,inplace=True)

    


date_current = datetime.now()
date_list = [date_current + timedelta(days=x)  for x in range(0, 7)]
date_new_list=list()
df_meteo=pd.read_csv('C:/Users/Administrator/Desktop/Produzione/VGAlimenti/Utils/meteo_bari',index_col=0)
df_meteo2=pd.DataFrame(columns=df_meteo.columns)
for i in range(0,len(date_list)):
    date_list[i]=date_list[i].replace(second=0, minute=0,hour=0).strftime('%Y-%m-%dT%H:%M:%S')
#chiamate API per ottenere il meteo
for i in date_list:
        dep_cit[citta]['date'][i]={"meteo":dict()}
        tm.sleep(1)
        fio = ForecastIO.ForecastIO(apikey,
                                        units=ForecastIO.ForecastIO.UNITS_SI,
                                        lang=ForecastIO.ForecastIO.LANG_ITALIAN,
                                        latitude=dep_cit[citta]['coordinate'][0], 
                                        longitude=dep_cit[citta]['coordinate'][1],
                                        time=i
                                   )
        dep_cit[citta]['date'][i]['meteo']=fio.daily['data'][0]

        
        df_comp=pd.DataFrame.from_dict(dep_cit[citta]['date'][str(i)],orient="index")
        df_comp['data_reg']=datetime.strptime(i,'%Y-%m-%dT%H:%M:%S').strftime('%Y-%m-%d %H:%M:%S')
        df_comp['citta']=citta
        df_meteo2=df_meteo2.append(df_comp)
 
df_meteo2['data_reg']= pd.to_datetime(df_meteo2['data_reg'],
                              format='%Y-%m-%d %H:%M:%S')
df['cod_mdep']=df['cod_mdep'].astype('int64')
df['data_reg']= pd.to_datetime(df['data_reg'],
                              format='%Y-%m-%d %H:%M:%S')
df = pd.merge(df, df_meteo2[['data_reg','apparentTemperatureMax','apparentTemperatureMin','cloudCover','humidity','precipIntensity','precipIntensityMax','precipProbability','windBearing','windSpeed','icon']],
               how='left',
               on=['data_reg'])  


clusters=pd.read_csv('C:/Users/Administrator/Desktop/Produzione/VGAlimenti/Clusters/clusters_7_daily', dtype={'cod_mart':str,'cod_mdep':str,'codAgente':str,'codPtoVend':str})    
clusters['cod_mdep']=clusters['cod_mdep'].astype('int64')
df = pd.merge(df,clusters,on=['cod_mdep','cod_mart','codAgente', 'descr_mgrp', 'descr_msgr', 'descr_msfa','codCliente','codPtoVend'],how='left')

df=df[~df.cluster.isna()]




#sales_mapping_dep = np.load('C:/Users/Administrator/Desktop/Carelli_Proposta_ordine/Algoritmi/dictionaries/sales_mapping_dep.npy').item()
#df['cod_mdep'] = df['cod_mdep'].map(sales_mapping_dep).astype(int)

unique_dict = np.load('C:/Users/Administrator/Desktop/Produzione/VGAlimenti/Dizionari/7_daily/unique_dict.npy',allow_pickle=True).item()
df['descr_mgrp'].fillna(0,inplace=True)
df['descr_msgr'].fillna(0,inplace=True)
df['descr_msfa'] .fillna(0,inplace=True)


agente = np.load('C:/Users/Administrator/Desktop/Produzione/VGAlimenti/Dizionari/7_daily/agente.npy',allow_pickle=True).item()
cliente = np.load('C:/Users/Administrator/Desktop/Produzione/VGAlimenti/Dizionari/7_daily/cliente.npy',allow_pickle=True).item()
ptoVend = np.load('C:/Users/Administrator/Desktop/Produzione/VGAlimenti/Dizionari/7_daily/ptoVend.npy',allow_pickle=True).item()
df['codAgente'] = df['codAgente'].map(agente).astype(int)
df['codCliente'] = df['codCliente'].map(cliente).astype(int)
df['codPtoVend'] = df['codPtoVend'].map(ptoVend).astype(int)

df['avg_prezzo_vendita'].fillna(0.0,inplace=True)
df['peso'].fillna(1.0,inplace=True)
df['promoNoPromo'].fillna(0.0,inplace=True)
df['variazione_prezzo'].fillna(0.0,inplace=True)
df['prezzo_vendita'].fillna(0.0,inplace=True)
df['apparentTemperatureMax'].fillna(0,inplace=True)
df['apparentTemperatureMin'].fillna(0,inplace=True)
df['cloudCover'].fillna(0,inplace=True)
df['humidity'].fillna(0,inplace=True)
df['precipIntensity'].fillna(0,inplace=True)
df['precipIntensityMax'].fillna(0,inplace=True)
df['precipProbability'].fillna(0,inplace=True)
df['windBearing'].fillna(0,inplace=True)
df['windSpeed'].fillna(0,inplace=True)

df_result=pd.DataFrame(columns=df[['cod_mart','cod_mdep','data_reg','descr_mgrp','descr_msfa']].columns)
startTime = datetime.now()

xg_reg= xgb.Booster({'nthread': 4})
xg_reg.load_model('C:/Users/Administrator/Desktop/Produzione/VGAlimenti/Modelli/7_daily/model.json')

notFirst=False

for i in range(len(date_list)):
    
    if notFirst:
        df=df[df.data_reg!=date_list[i-1]]
        df=pd.concat([df,subset], ignore_index=True)
        df=pd.merge(df,df_target, on=['cod_mart','codAgente','codCliente','codPtoVend'], how= 'left')
        df.day1.fillna(df.day1_ptoVend, inplace=True)
        df.day2.fillna(df.day2_ptoVend, inplace=True)
        df.day3.fillna(df.day3_ptoVend, inplace=True)
        df.day4.fillna(df.day4_ptoVend, inplace=True)
        df.day5.fillna(df.day5_ptoVend, inplace=True)
        df.day6.fillna(df.day6_ptoVend, inplace=True)
        df.day7.fillna(df.day7_ptoVend, inplace=True)
        df.drop(['day1_ptoVend','day2_ptoVend','day3_ptoVend','day4_ptoVend','day5_ptoVend','day6_ptoVend','day7_ptoVend'],axis=1, inplace=True)
        df.drop(['qta_vendutaAgente_GGprec','qta_vendutaPtoVend_GGprec','qta_venduta_GGprec','qta_venduta_settPrec','qta_vendutaCliente_GGprec'],axis=1, inplace=True)
        df=df.rename(columns={'day1':'day1_ptoVend','day2':'day2_ptoVend','day3':'day3_ptoVend','day4':'day4_ptoVend','day5':'day5_ptoVend','day6':'day6_ptoVend','day7':'day7_ptoVend'})
        df=creaVariabile_qtaVend_GGprec(df)
        df=creaVariabile_qtaVend_GGprec_Agente(df)
        df=creaVariabile_qtaVend_GGprec_PtoVend(df)
        df=creaVariabile_qtaVend_GGprec_codCliente(df)
        df=creaVariabile_qtaVend_SETTprec(df)
        df['qta_vendutaAgente_GGprec'].fillna(0,inplace=True)
        df['qta_vendutaPtoVend_GGprec'].fillna(0,inplace=True)
        df['qta_venduta_GGprec'].fillna(0,inplace=True)
        df['qta_venduta_settPrec'].fillna(0,inplace=True)
        df['qta_vendutaCliente_GGprec'].fillna(0,inplace=True)
        
        
    df['codAgente']=df['codAgente'].astype('int64')
    df['codCliente']=df['codCliente'].astype('int64')
    df['codPtoVend']=df['codPtoVend'].astype('int64')    
    subset=df[df.data_reg==datetime.strptime(date_list[i], '%Y-%m-%dT%H:%M:%S').strftime('%Y-%m-%d')]    
    df_result_tmp=subset[['cod_mart','cod_mdep','data_reg','descr_mgrp','descr_msfa']]
    
    X_test=subset[['cod_mdep', 'mese', 'ggmese', 'settimana', 'GGsettimana', 'ricorrenza',
           'codAgente', 'codCliente', 'codPtoVend', 'peso', 'qta_cnf',
           'gg_scad_partita', 'promoNoPromo', 'sum_promo',
           'QtaMediaVendGiorn_ptoVend', 'PrezzoMedioVendGiorn', 'day1_ptoVend',
           'day2_ptoVend', 'day3_ptoVend', 'day4_ptoVend', 'day5_ptoVend',
           'day6_ptoVend', 'day7_ptoVend', 'prezzo_vendita', 'avg_prezzo_vendita',
           'min_prezzo_vendita', 'max_prezzo_vendita', 'variazione_prezzo',
           'avg_variazione_prezzo', 'min_variazione_prezzo',
           'max_variazione_prezzo', 'stdev_variazione_prezzo', 'dev_std_30',
           'media_30', 'media_15', 'media_7', 'cluster', 'qta_venduta_GGprec',
           'qta_vendutaAgente_GGprec', 'qta_vendutaPtoVend_GGprec',
           'qta_vendutaCliente_GGprec', 'qta_venduta_settPrec',
           'apparentTemperatureMax', 'apparentTemperatureMin', 'cloudCover',
           'humidity', 'precipIntensity', 'precipIntensityMax',
           'precipProbability', 'windBearing', 'windSpeed', 'descr_mgrp',
           'descr_msgr', 'descr_msfa']]
    
    X_test['cod_mdep']=0
    X_test['descr_mgrp'] = X_test['descr_mgrp'].map(unique_dict).astype(int)
    X_test['descr_msgr'] = X_test['descr_msgr'].map(unique_dict).astype(int)
    X_test['descr_msfa'] = X_test['descr_msfa'].map(unique_dict).astype(int)
    
    data_dmatrix = xgb.DMatrix(data=X_test)
    y_val= xg_reg.predict(data_dmatrix)
    
    df_result_tmp['predizione']=np.around(y_val)
    df_result_tmp['data_predizione']=date_current_str_m1
    
    subset['qta_venduta']=np.around(y_val)

    days_target=subset[['cod_mart','codAgente','codCliente','codPtoVend','day1_ptoVend', 'day2_ptoVend', 'day3_ptoVend', 'day4_ptoVend','day5_ptoVend',
                    'day6_ptoVend', 'day7_ptoVend']].values
    days_target=np.insert(days_target,4,np.around(y_val),axis=1)
    days_target=days_target[:,:-1]
    df_target= pd.DataFrame(data=days_target, columns=['cod_mart','codAgente','codCliente','codPtoVend','day1', 'day2', 'day3', 'day4','day5',
                    'day6', 'day7'])
    df_result= df_result.append(df_result_tmp)
    notFirst=True
         
          
df_result=df_result.groupby(['cod_mdep','data_reg','cod_mart','descr_mgrp','descr_msfa','data_predizione']).sum().reset_index()      


df_mail=df_result.drop(['data_reg','data_predizione'], axis=1).sort_values(by=['descr_mgrp','descr_msfa','cod_mart','cod_mdep'])

df_result.drop(['descr_mgrp','descr_msfa'], axis=1, inplace=True)     
endTime = datetime.now()
engine = sqlalchemy.create_engine("mssql+pyodbc://" + 'sa' + ":" + 'sa' + "@" + server + "/" + 'vgalimenti_NN_project' + "?driver=SQL+Server")
df_result.to_sql("Predizione",engine,if_exists='append',index=False)
print('Execution: ' + str(endTime - startTime))

ref=pd.read_excel('C:/Users/Administrator/Desktop/Produzione/VGAlimenti/Utils/referenze_da_esaminare.xlsx')
df_mail=df_mail[df_mail.cod_mart.isin(ref.cod_mart)]
writer = pd.ExcelWriter("predizioni_giornaliere.xlsx", engine='openpyxl')
df_mail.to_excel(writer, sheet_name='Predizioni_giornaliere',index=False)  # send df to writer
writer.sheets['Predizioni_giornaliere'].column_dimensions['A'].width = 15
writer.sheets['Predizioni_giornaliere'].column_dimensions['B'].width = 15
writer.sheets['Predizioni_giornaliere'].column_dimensions['C'].width = 50
writer.sheets['Predizioni_giornaliere'].column_dimensions['D'].width = 50
writer.sheets['Predizioni_giornaliere'].column_dimensions['E'].width = 20

writer.save()
#send_email('vincenzomorelli@live.com','Predizioni_giornaliere_'+date_current_str_m1,('Buongiorno Antonio, \n\nin allegato puoi trovare le previsioni giornaliere aggiornate ad oggi:  ' + date_current_str_m1 +'.\n\nBuona Giornata,\n\nVincenzo'),'C:/Users/Administrator/Desktop/VG_Project/Agloritmi/predizioni_giornaliere.xlsx')    

      
        

                       