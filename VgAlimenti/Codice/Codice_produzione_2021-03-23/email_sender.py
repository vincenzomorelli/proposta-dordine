# -*- coding: utf-8 -*-
"""
Created on Tue Jul 21 12:20:15 2020

@author: Administrator
"""

import smtplib 
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders 
import os

fromaddr = "predictions.morelli@gmail.com"
toaddr = "EMAIL address of the receiver"
password='Prediction1'
   
# instance of MIMEMultipart 

def send_email(toaddr,subject,body,fileToSend):
    msg = MIMEMultipart() 
    
    filename=os.path.basename(fileToSend)
    # storing the senders email address   
    msg['From'] = fromaddr 
      
    # storing the receivers email address  
    msg['To'] = toaddr 
      
    # storing the subject  
    msg['Subject'] = subject
      
    # string to store the body of the mail 
    body =body
      
    # attach the body with the msg instance 
    msg.attach(MIMEText(body, 'plain')) 
      
    # open the file to be sent  
    attachment = open(filename, "rb") 
      
    # instance of MIMEBase and named as p 
    p = MIMEBase('application', 'octet-stream') 
      
    # To change the payload into encoded form 
    p.set_payload((attachment).read()) 
      
    # encode into base64 
    encoders.encode_base64(p) 
       
    p.add_header('Content-Disposition', "attachment; filename= %s" % filename) 
      
    # attach the instance 'p' to instance 'msg' 
    msg.attach(p) 
      
    # creates SMTP session 
    s = smtplib.SMTP('smtp.gmail.com', 587) 
      
    # start TLS for security 
    s.starttls() 
      
    # Authentication 
    s.login(fromaddr,password) 
      
    # Converts the Multipart msg into a string 
    text = msg.as_string() 
      
    # sending the mail 
    s.sendmail(fromaddr, toaddr, text) 
      
    # terminating the session 
    s.quit() 
    
    print('email sent!')
    

#send_email('vincenzomorelli@live.com','Subject','testmail','C:/Users/Administrator/Desktop/VG_Project/Agloritmi/previsioni/file_di_prova.txt')
