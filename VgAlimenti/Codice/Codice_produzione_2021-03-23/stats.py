# -*- coding: utf-8 -*-
"""
Created on Tue Feb  2 09:49:23 2021

@author: VincenzoM

Funzioni che semplificano il calcolo delle features statistiche 
"""

import pandas as pd
from joblib import Parallel, delayed
import multiprocessing
"""
df = dataframe pandas
new_feature = nome della nuova feature che si vuole creare
group = gruppo delle variabili che delineano i limiti della feature !! L'ORDINE DELLA LISTSA DI GROUP CONTA PER IL SORT
target = feature da raggruppare
operation = operazione di raggruppamento
shift = 1 di default perchè il giorno di oggi non va MAI considerato in quanto non disponibile

"""
def shifted_standard_feature(df,new_feature,group,target,operation='sum',shift=1,fillna=0):
    
    grouped_list=list(group)
    grouped_list.append(new_feature)
    
    if operation=='sum':
        grouped_df = df.groupby(group,as_index=False)[target].sum()
        #print(grouped_df.head())
        
    elif operation=='max':
        grouped_df = df.groupby(group,as_index=False)[target].max()
        
    elif operation=='min':
        grouped_df = df.groupby(group,as_index=False)[target].min()
        
    elif operation=='mean':
        grouped_df = df.groupby(group,as_index=False)[target].mean()
    
    grouped_df[new_feature] = grouped_df.sort_values(group)[target].shift(shift).fillna(fillna)

    df1 = pd.merge(df, grouped_df[grouped_list],how='left',on=group)
    
    return df1


"""
df = dataframe pandas
new_feature = nome della nuova feature che si vuole creare
sort = ordinamento secondo il quale  eseguire lo shifting
target = feature da shiftare
group = gruppo delle variabili che delineano i limiti della feature
shift = 1 di default perchè il giorno di oggi non va MAI considerato in quanto non disponibile

"""

def simple_shift(df,new_feature,sort,group,target,shift=1):
        
    #delete duplicates
    grouped_list_d=list(sort)
    grouped_list_d.append(target)
    
    #merge with sorted list and new features
    grouped_list_merge=list(sort)
    grouped_list_merge.append(new_feature)
    
    grouped_df = df[grouped_list_d].drop_duplicates()
    grouped_df[new_feature]=grouped_df.sort_values(sort).groupby(group)[target].shift(shift)
    df = pd.merge(df, grouped_df[grouped_list_merge],how='left',on=sort)
    return df


"""
df = dataframe pandas
new_feature = nome della nuova feature che si vuole creare
sort = ordinamento secondo il quale  eseguire lo shifting
group = gruppo delle variabili che delineano i limiti della feature
target = feature da shiftare e gruppare
operation = operazione di raggruppamento
preceding= numero di valori da rollare
shift = 1 di default perchè il giorno di oggi non va MAI considerato in quanto non disponibile
fillna = riempie i nulli

"""

def rolling_feature(df,new_feature,sort,group,target,operation,preceding,shift,fillna=0):
    
    df.sort_values(sort,inplace=True)
    df['feat_1'] = df.sort_values(sort).groupby(group)[target].shift(shift)

    if operation=='sum':
        df[new_feature]=df.sort_values(sort).groupby(group)['feat_1'].rolling(preceding,min_periods=1).sum().fillna(fillna).values
            
    elif operation=='max':
        df[new_feature]=df.sort_values(sort).groupby(group)['feat_1'].rolling(preceding,min_periods=1).max().fillna(fillna).values
            
    elif operation=='min':
        df[new_feature]=df.sort_values(sort).groupby(group)['feat_1'].rolling(preceding,min_periods=1).min().fillna(fillna).values
            
    elif operation=='mean':
        df[new_feature]=df.sort_values(sort).groupby(group)['feat_1'].rolling(preceding,min_periods=1).mean().fillna(fillna).values
    
    df.drop('feat_1',axis=1,inplace=True)      
    
    return df


#examples
#data = pd.read_csv('dati/1_daily.csv',sep=';',dtype={'cod_mart':str,'cod_mdep':str,'codAgente':str,'codPtoVend':str})
#data=data[data.cod_mart=='080100420']
#data['anno'] = data['data_reg'].str[:4]
#dfa=shifted_standard_feature(data,'qta_venduta_GGprecmsgr',['cod_mdep','data_reg','descr_mgrp'],'qta_stimata','sum',1)
#dfa=shifted_standard_feature(dfa,'lalla2',['cod_mdep','cod_mart','anno','settimana'],'qta_stimata',shift=0)
#dfa=rolling_feature(data,'lalla3',['cod_mdep','cod_mart','data_reg'],['cod_mdep','cod_mart'],'qta_stimata','sum',preceding=3,shift=3)
