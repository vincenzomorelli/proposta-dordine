# -*- coding: utf-8 -*-
"""
Created on Thu Jul  2 17:01:06 2020

@author: Administrator
"""

## -*- coding: utf-8 -*-
#"""
##Created on Mon Oct 29 09:03:33 2018
##
##@author: Vincenzo Morelli
##"""
from import_modules import *
from email_sender import *
from stats import * 

def RMSLE(y_true, y_pred):
        return K.sqrt(K.mean(K.square(K.log(1+y_true) - K.log(1+y_pred)))) 

server = '192.168.102.214\SQL2014'
database ='vgalimenti_NN_project'
username = 'sa'
password = 'sa'
connectionString = 'DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+password


apikey = '679f07cc6e387621a41b11bf17989d90'
citta='BARI'
dep_cit = np.load('C:/Users/Administrator/Desktop/Produzione/VGAlimenti/Utils/dep_cit.npy',allow_pickle=True).item()


date_current_m1= datetime.now()+ timedelta(0)
date_current_str_m1=date_current_m1.strftime('%Y-%m-%d')

cnxn = pyodbc.connect(connectionString)
start=str((datetime.now()+ timedelta(1)).weekday()+1)
df=pd.read_sql_query("SET DATEFIRST " +start+"; "
+"with tmp as (SELECT cod_mart,"
+"  cod_mdep ,"
+"  data_reg,"
+"  mese,"
+"  ggmese,"
+"  settimana,"
+"  GGsettimana,"
+"  ricorrenza,"
+"  [peso],"
+"  [descr_mgrp],"
+"  [descr_msgr],"
+"  [descr_msfa],"
+"  qta_cnf,"
+"  gg_scad_partita,"
+"  sum(promoNoPromo) as promoNoPromo,"
+"  sum(sum_promo) as sum_promo,"
+"  isnull(avg(sum(qta_venduta)) over (partition by  DATEPART( WEEKDAY ,ab.data_reg ),ab.cod_mdep,ab.cod_mart order by ab.cod_mdep,ab.cod_mart,ab.data_reg ROWS BETWEEN 5 PRECEDING AND 1 PRECEDING),isnull(sum(qta_venduta),0))  as QtaMediaVendGiorn,"
+"  isnull(avg(avg(avg_prezzo_vendita)) over (partition by  DATEPART( WEEKDAY ,ab.data_reg ),ab.cod_mdep,ab.cod_mart order by ab.cod_mdep,ab.cod_mart,ab.data_reg ROWS BETWEEN 5 PRECEDING AND 1 PRECEDING),avg(avg_prezzo_vendita)) as PrezzoMedioVendGiorn,"
+"	LAG(sum(qta_venduta),1,0) OVER (Partition by ab.cod_mdep,ab.cod_mart order by ab.cod_mdep,ab.cod_mart,ab.data_reg,mese,GGmese,GGsettimana) as day1_ptoVend,"
+"	LAG(sum(qta_venduta),2,0) OVER (Partition by ab.cod_mdep,ab.cod_mart order by ab.cod_mdep,ab.cod_mart,ab.data_reg,mese,GGmese,GGsettimana) as day2_ptoVend,"
+"	LAG(sum(qta_venduta),3,0) OVER (Partition by ab.cod_mdep,ab.cod_mart order by ab.cod_mdep,ab.cod_mart,ab.data_reg,mese,GGmese,GGsettimana) as day3_ptoVend,"
+"	LAG(sum(qta_venduta),4,0) OVER (Partition by ab.cod_mdep,ab.cod_mart order by ab.cod_mdep,ab.cod_mart,ab.data_reg,mese,GGmese,GGsettimana) as day4_ptoVend,"
+"	LAG(sum(qta_venduta),5,0) OVER (Partition by ab.cod_mdep,ab.cod_mart order by ab.cod_mdep,ab.cod_mart,ab.data_reg,mese,GGmese,GGsettimana) as day5_ptoVend,"
+"	LAG(sum(qta_venduta),6,0) OVER (Partition by ab.cod_mdep,ab.cod_mart order by ab.cod_mdep,ab.cod_mart,ab.data_reg,mese,GGmese,GGsettimana) as day6_ptoVend,"
+"	LAG(sum(qta_venduta),7,0) OVER (Partition by ab.cod_mdep,ab.cod_mart order by ab.cod_mdep,ab.cod_mart,ab.data_reg,mese,GGmese,GGsettimana) as day7_ptoVend,"
+"  avg(prezzo_vendita) prezzo_vendita,"
+"  min(min_prezzo_vendita) min_prezzo_vendita,"
+"  max(max_prezzo_vendita) max_prezzo_vendita,"
+"  avg(variazione_prezzo)variazione_prezzo,"
+"  min(min_variazione_prezzo)min_variazione_prezzo,"
+"  avg(stdev_variazione_prezzo)stdev_variazione_prezzo,"
+"  max(max_variazione_prezzo) as max_variazione_prezzo,"
+"  isnull(STDEV(sum(qta_venduta)) OVER(Partition by ab.cod_mdep,ab.cod_mart order by ab.cod_mdep,ab.cod_mart,ab.data_reg,mese,GGmese,GGsettimana ROWS BETWEEN 18 PRECEDING AND 1 PRECEDING),0) as dev_std_30,"
+"  isnull(AVG(sum(qta_venduta)) OVER(Partition by ab.cod_mdep,ab.cod_mart order by ab.cod_mdep,ab.cod_mart,ab.data_reg,mese,GGmese,GGsettimana ROWS BETWEEN 18 PRECEDING AND 1 PRECEDING),0) as media_30,"
+"  isnull(AVG(sum(qta_venduta)) OVER(Partition by ab.cod_mdep,ab.cod_mart order by ab.cod_mdep,ab.cod_mart,ab.data_reg,mese,GGmese,GGsettimana ROWS BETWEEN 9 PRECEDING AND 1 PRECEDING),0) as media_15,"
+"  isnull(AVG(sum(qta_venduta)) OVER(Partition by ab.cod_mdep,ab.cod_mart order by ab.cod_mdep,ab.cod_mart,ab.data_reg,mese,GGmese,GGsettimana ROWS BETWEEN 6 PRECEDING AND 1 PRECEDING),0) as media_7"
+"   ,sum([QuantitaSostituita])[QuantitaSostituita]"
+"   ,sum(QuantitaResa)QuantitaResa"
+"   ,sum([ConfRese])[ConfRese]"
+"   ,sum([QuantitaOmaggi])[QuantitaOmaggi]"
+"   ,isnull(sum(inevaso),0) as inevaso,"
+"  sum(qta_venduta)qta_venduta"
+"  from analisi_vendite ab"
+ " Where data_reg<dateadd(day,8,'"+date_current_str_m1+"')"
+"  group by cod_mart,cod_mdep ,data_reg,mese,GGmese,settimana,GGsettimana,ricorrenza,[peso],[descr_mgrp],[descr_msgr],[descr_msfa],qta_cnf,gg_scad_partita"
+")"
+" SELECT cod_mart, "
+" 	cod_mdep , "
+" 	DATEPART(YEAR,data_reg) as anno, "
+" 	DATEPART(week,data_reg) as settimana, "
+" 	sum(ricorrenza) as ricorrenza, "
+" 	[peso], "
+" 	[descr_mgrp], "
+" 	[descr_msgr], "
+" 	[descr_msfa], "
+" 	min(qta_cnf) as qta_cnf, "
+" 	min(gg_scad_partita) as gg_scad_partita , "
+" 	sum(promoNoPromo) as promoNoPromo, "
+" 	sum(sum_promo) as sum_promo, "
+" 	sum(QtaMediaVendGiorn)  QtaMediaVendGiorn, "
+" 	avg (PrezzoMedioVendGiorn) as PrezzoMedioVendGiorn, "
+" 	LAG(sum(qta_venduta),1,0) OVER (Partition by cod_mdep,cod_mart order by cod_mdep,cod_mart,DATEPART(YEAR,data_reg),DATEPART(week,data_reg)) as week1_ptoVend, "
+" 	LAG(sum(qta_venduta),2,0) OVER (Partition by cod_mdep,cod_mart order by cod_mdep,cod_mart,DATEPART(YEAR,data_reg),DATEPART(week,data_reg)) as week2_ptoVend, "
+" 	LAG(sum(qta_venduta),3,0) OVER (Partition by cod_mdep,cod_mart order by cod_mdep,cod_mart,DATEPART(YEAR,data_reg),DATEPART(week,data_reg)) as week3_ptoVend, "
+" 	LAG(sum(qta_venduta),4,0) OVER (Partition by cod_mdep,cod_mart order by cod_mdep,cod_mart,DATEPART(YEAR,data_reg),DATEPART(week,data_reg)) as week4_ptoVend, "
+" 	LAG(sum(qta_venduta),5,0) OVER (Partition by cod_mdep,cod_mart order by cod_mdep,cod_mart,DATEPART(YEAR,data_reg),DATEPART(week,data_reg)) as week5_ptoVend, "
+" 	LAG(sum(qta_venduta),6,0) OVER (Partition by cod_mdep,cod_mart order by cod_mdep,cod_mart,DATEPART(YEAR,data_reg),DATEPART(week,data_reg)) as week6_ptoVend, "
+" 	LAG(sum(qta_venduta),7,0) OVER (Partition by cod_mdep,cod_mart order by cod_mdep,cod_mart,DATEPART(YEAR,data_reg),DATEPART(week,data_reg)) as week7_ptoVend, "
+" 	avg(prezzo_vendita) as prezzo_vendita, "
+" 	avg(min_prezzo_vendita) as min_prezzo_vendita, "
+" 	avg(max_prezzo_vendita) as max_prezzo_vendita, "
+" 	avg(variazione_prezzo) as variazione_prezzo, "
+" 	avg(min_variazione_prezzo) as min_variazione_prezzo, "
+" 	avg(max_variazione_prezzo) as max_variazione_prezzo, "
+" 	avg(stdev_variazione_prezzo) as stdev_variazione_prezzo, "
+" 	LAG(sum(dev_std_30),1,0) OVER (Partition by cod_mdep,cod_mart order by cod_mdep,cod_mart,DATEPART(YEAR,data_reg),DATEPART(week,data_reg)) as dev_std_30, "
+" 	LAG(sum(media_30),1,0) OVER (Partition by cod_mdep,cod_mart order by cod_mdep,cod_mart,DATEPART(YEAR,data_reg),DATEPART(week,data_reg)) as media_30, "
+" 	LAG(sum(media_15),1,0) OVER (Partition by cod_mdep,cod_mart order by cod_mdep,cod_mart,DATEPART(YEAR,data_reg),DATEPART(week,data_reg)) as media_15, "
+" 	LAG(sum(media_7),1,0) OVER (Partition by cod_mdep,cod_mart order by cod_mdep,cod_mart,DATEPART(YEAR,data_reg),DATEPART(week,data_reg)) as media_7, "
+" 	sum([QuantitaSostituita]) as [QuantitaSostituita] "
+" 	,sum(inevaso) as inevaso "
+"    ,sum(QuantitaResa) as QuantitaResa "
+"    ,sum([ConfRese]) as [ConfRese] "
+"    ,sum([QuantitaOmaggi]) as [QuantitaOmaggi] "
+" 	,sum(qta_venduta)   as qta_venduta "
+"   FROM    tmp"
+"   group by cod_mdep,cod_mart,DATEPART(YEAR,data_reg),DATEPART(week,data_reg),[peso], "
+"   descr_mgrp,descr_msgr,descr_msfa"
+"    order by cod_mdep,cod_mart,anno,DATEPART(week,data_reg)",cnxn)

df=df[df.qta_venduta>=0]
#DATEADD(month,-3,GETDATE())


clusters=pd.read_csv('C:/Users/Administrator/Desktop/Produzione/VGAlimenti/Clusters/clusters_1_weekly',dtype={'cod_mart':str,'cod_mdep':str})    
#df['cod_mart']=df.cod_mart.where(df.cod_mart.str[:1]!=str(0),df.cod_mart.str[1:])
df = pd.merge(df,clusters,on=['cod_mdep','cod_mart', 'descr_mgrp', 'descr_msgr', 'descr_msfa'],how='left')
df['cod_mart']=df['cod_mart'].astype(str)
df=df[~df.cluster.isna()]
df['cod_mdep']=df['cod_mdep'].astype('int64')


df=shifted_standard_feature(df,'qta_venduta_prec_mgrp_sum',['cod_mdep','descr_mgrp','anno','settimana'],'qta_venduta','sum',1)
df=shifted_standard_feature(df,'qta_venduta_prec_msgr_sum',['cod_mdep','descr_msgr','anno','settimana'],'qta_venduta','sum',1)
df=shifted_standard_feature(df,'qta_venduta_prec_msfa_sum',['cod_mdep','descr_msfa','anno','settimana'],'qta_venduta','sum',1)
df=shifted_standard_feature(df,'qta_venduta_prec_mgrp_mean',['cod_mdep','descr_mgrp','anno','settimana'],'qta_venduta','mean',1)
df=shifted_standard_feature(df,'qta_venduta_prec_msgr_mean',['cod_mdep','descr_msgr','anno','settimana'],'qta_venduta','mean',1)
df=shifted_standard_feature(df,'qta_venduta_prec_msfa_mean',['cod_mdep','descr_msfa','anno','settimana'],'qta_venduta','mean',1)
df=shifted_standard_feature(df,'qta_venduta_prec_mgrp_max',['cod_mdep','descr_mgrp','anno','settimana'],'qta_venduta','max',1)
df=shifted_standard_feature(df,'qta_venduta_prec_msgr_max',['cod_mdep','descr_msgr','anno','settimana'],'qta_venduta','max',1)
df=shifted_standard_feature(df,'qta_venduta_prec_msfa_max',['cod_mdep','descr_msfa','anno','settimana'],'qta_venduta','max',1)
df=rolling_feature(df,'qta_venduta_prec_anno_mean',['cod_mdep','cod_mart','settimana','anno'],['cod_mdep','cod_mart','settimana'],'qta_venduta','mean',3,1)
df=df[df.anno==datetime.now().year]
df=df[df.settimana==df.settimana.max()]

unique_dict = np.load('C:/Users/Administrator/Desktop/Produzione/VGAlimenti/Dizionari/1_weekly/unique_dict.npy',allow_pickle=True).item()

df['descr_mgrp'].fillna(0,inplace=True)
df['descr_msgr'].fillna(0,inplace=True)
df['descr_msfa'] .fillna(0,inplace=True)



unique_df=pd.DataFrame.from_dict(unique_dict, orient='index').reset_index()
unique_df.rename(columns={'index':'descr_join'}, inplace=True)
df['descr_join']=df['descr_mgrp'].astype(str)+" "+df['descr_msgr'].astype(str)+" "+df['descr_msfa'].astype(str)
df.drop(['descr_mgrp','descr_msgr','descr_msfa'], axis=1, inplace=True)
df = df.merge(unique_df,on=['descr_join'],how='left')

df.drop('descr_join',axis=1,inplace=True)    


df['peso'].fillna(1.0,inplace=True)
df['promoNoPromo'].fillna(0.0,inplace=True)
df['variazione_prezzo'].fillna(0.0,inplace=True)
df['prezzo_vendita'].fillna(0.0,inplace=True)
df.fillna(0)

df_result=df[['cod_mart','cod_mdep','anno','settimana','descr_mgrp','descr_msfa']]
startTime = datetime.now()
X_test=df[['cod_mdep', 'settimana', 'ricorrenza', 'peso', 'qta_cnf',
       'gg_scad_partita', 'promoNoPromo', 'sum_promo', 'QtaMediaVendGiorn',
       'PrezzoMedioVendGiorn', 'week1_ptoVend', 'week2_ptoVend',
       'week3_ptoVend', 'week4_ptoVend', 'week5_ptoVend', 'week6_ptoVend',
       'week7_ptoVend', 'prezzo_vendita', 'min_prezzo_vendita',
       'max_prezzo_vendita', 'variazione_prezzo', 'max_variazione_prezzo',
       'stdev_variazione_prezzo', 'dev_std_30', 'media_30', 'media_15',
       'media_7', 'cluster', 'qta_venduta_prec_mgrp_sum',
       'qta_venduta_prec_msgr_sum', 'qta_venduta_prec_msfa_sum',
       'qta_venduta_prec_mgrp_mean', 'qta_venduta_prec_msgr_mean',
       'qta_venduta_prec_msfa_mean', 'qta_venduta_prec_mgrp_max',
       'qta_venduta_prec_msgr_max', 'qta_venduta_prec_msfa_max',
       'qta_venduta_prec_anno_mean', 'descr_mgrp', 'descr_msgr', 'descr_msfa',
       '0', '1', '2', '3', '4', '5', '6', '7', '8', '9']]

X_test['cod_mdep']=0
X_test.cluster=X_test.cluster.astype('int32')
X_test.promoNoPromo=X_test.promoNoPromo.astype('int32')
X_test.qta_cnf=X_test.qta_cnf.astype('int32')

#xg_reg= xgb.Booster({'nthread': 4})
from catboost import CatBoostRegressor
model='C:/Users/Administrator/Desktop/Produzione/VGAlimenti/Modelli/1_weekly/catboost_'+str(start)
bst=CatBoostRegressor()
bst = bst.load_model(model)




y_val= bst.predict(X_test)
    
df_result['predizione']=abs(y_val)
df_result['data_predizione']=date_current_str_m1

df_result=df_result.groupby(['cod_mdep','anno','settimana','cod_mart','descr_mgrp','descr_msfa','data_predizione']).sum().reset_index()      
#df_result.to_excel("output.xlsx")
df_result['predizione']=np.around(df_result.predizione)

df_mail=df_result.drop(['anno','data_predizione','settimana'], axis=1).sort_values(by=['descr_mgrp','descr_msfa','cod_mart','cod_mdep'])

df_result.drop(['descr_mgrp','descr_msfa'], axis=1, inplace=True) 
df_result['giorno']=start    
endTime = datetime.now()
engine = sqlalchemy.create_engine("mssql+pyodbc://" + 'sa' + ":" + 'sa' + "@" + server + "/" + 'vgalimenti_NN_project' + "?driver=SQL+Server")
df_result.to_sql("Predizione_settimanale",engine,if_exists='append',index=False)
print('Execution: ' + str(endTime - startTime))



        
           
		
      
        

                       